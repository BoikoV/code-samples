<?php
use kartik\mpdf\Pdf;

require __DIR__ . '/container.php';
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'language' => 'en',
    'charset' => 'utf-8',
    'timeZone' => 'America/New_York',
    'components' => [
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'timeZone' => 'America/New_York',
            'defaultTimeZone' => 'America/New_York',
            'locale' => 'en-US',
            'dateFormat' => 'php:m/d/Y',
            'timeFormat' => 'php:H:i',
        ],
        'cache' => [
            'class' => 'yii\caching\DbCache',
        ],
        'session' => [
            'class' => 'yii\web\DbSession',
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        ],
        'urlManagerFront' => [
            'class' => 'yii\web\UrlManager',
            'baseUrl' => 'http://keepmore.local',
            'showScriptName' => false,
            'enablePrettyUrl' => true,
        ],
        'urlManagerBack' => [
            'class' => 'yii\web\UrlManager',
            'baseUrl' => 'http://admin.keepmore.local',
            'showScriptName' => false,
            'enablePrettyUrl' => true,
        ],
        'urlManagerApp' => [
            'class' => 'yii\web\UrlManager',
            'baseUrl' => 'http://app.keepmore.local',
            'showScriptName' => false,
            'enablePrettyUrl' => true,
        ],
        'helper' => [
            'class' => 'common\components\Helper',
        ],
        'pdf' => [
            'class' => Pdf::classname(),
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'cssFile' => '@vendor/bower/bootstrap/dist/css/bootstrap.css',

        ],

    ],
];
