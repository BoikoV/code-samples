<?php

namespace common\models\frontend;

use Yii;
use common\models\backend\Address;

/**
 * This is the model class for table "{{%ACCOUNTANT_CLIENTS}}".
 *
 * @property string $client_id
 * @property string $username
 * @property string $date_added
 * @property string $address_id
 * @property string $active
 * @property string $last_access
 */
class AccountantClient extends \common\components\AppActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ACCOUNTANT_CLIENTS}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'client_id',
                    'active'
                ],
                'required'
            ],
            [
                [
                    'date_added',
                    'last_access'
                ],
                'safe'
            ],
            [
                [
                    'client_id',
                    'address_id'
                ],
                'string',
                'max' => 32
            ],
            [
                ['username'],
                'string',
                'max' => 55
            ],
            [
                ['active'],
                'string',
                'max' => 1
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'client_id' => 'Client ID',
            'username' => 'Username',
            'date_added' => 'Date Added',
            'address_id' => 'Address ID',
            'active' => 'Active',
            'last_access' => 'Last Access',
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddress()
    {
        return $this->hasOne(Address::className(), ['id' => 'address_id']);
    }
}
