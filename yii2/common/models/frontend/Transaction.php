<?php

namespace common\models\frontend;

use Yii;
use common\models\backend\Category;
use common\components\Helper;

/**
 * This is the model class for table "{{%TRANSACTION}}".
 *
 * @property string $id
 * @property string $account_id
 * @property string $type
 * @property string $date
 * @property string $date_entered
 * @property string $date_modified
 * @property string $source_id
 * @property string $check_number
 * @property string $method_id
 * @property string $category_id
 * @property string $custom_tag_id
 * @property double $amount
 * @property string $split
 * @property string $master_id
 * @property string $inventory
 * @property string $auto
 * @property string $auto_id
 * @property string $memo
 * @property string $reconciled
 * @property string $reconciled_date
 * @property string $locked
 * @property string $locked_date
 * @property string $approved
 * @property string $approved_date
 * @property string $accountant_changed
 * @property string $ref_source
 * @property string $ref_type
 * @property string $ref_code
 *
 * @property BankAccount $bankAccount
 * @property Source $source
 * @property Category $category
 * @property CustomTag $customTag
 */
class Transaction extends \common\components\AppActiveRecord
{
    const TYPE_FUNDS_IN = 'I';
    const TYPE_FUNDS_OUT = 'E';
    const TYPE_BEGINNING_BALANCE = 'B';

    const METHOD_CASH = 'Cash';
    const METHOD_CHECK = 'Check';
    const METHOD_CREDIT_CARD = 'Credit Card';
    const METHOD_DEBIT_CARD = 'Debit Card';
    const METHOD_DEPOSIT = 'Deposit';
    const METHOD_ELECTRONIC = 'Electronic';
    const METHOD_RETURN_REFUND = 'Return/Refund';
    const METHOD_BEGINNING_BALANCE = 'Beginning Balance';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%TRANSACTION}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            [

                [
                    'date',
                    'type',
                    'account_id',
                    'source_id',
                    'method_id',
                    'category_id',
                    'custom_tag_id',
                    'amount'
                ],
                'required',
                'on' => [
                    self::TYPE_FUNDS_IN,
                    self::TYPE_FUNDS_OUT,
                ]
            ],
            [
                [
                    'date',
                    'type',
                    'account_id',
                    'amount'
                ],
                'required',
                'on' => [
                    self::TYPE_BEGINNING_BALANCE,
                ]
            ],
            [
                [
                    'check_number',
                ],
                'required',
                'when' => function ($model) {
                    return ($model->method_id=="Check");
                },
                'whenClient' => "function (attribute, value) {
                    return $('#transaction-method_id').val() == 'Check';
                }",
                'message' => 'Check Number cannot be blank.',
            ],
            [
                [
                    'date_entered',
                    'date_modified',
                    'reconciled_date',
                    'locked_date',
                    'approved_date'
                ],
                'safe'
            ],
            [
                ['amount'],
                'number'
            ],
            [
                [
                    'ref_source',
                    'ref_type'
                ],
                'string'
            ],
            [
                [
                    'id',
                    'account_id',
                    'source_id',
                    'method_id',
                    'category_id',
                    'custom_tag_id',
                    'master_id',
                    'auto_id',
                    'accountant_changed'
                ],
                'string',
                'max' => 32
            ],
            [
                [
                    'type',
                    'split',
                    'inventory',
                    'auto',
                    'reconciled',
                    'locked',
                    'approved'
                ],
                'string',
                'max' => 1
            ],
            [
                ['check_number'],
                'string',
                'max' => 10
            ],
            [
                ['memo'],
                'string',
                'max' => 255
            ],
            [
                ['memo'],
                'trim',
            ],
            [
                ['ref_code'],
                'string',
                'max' => 50
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'account_id' => 'Account',
            'type' => 'Transacton Type',
            'date' => 'Date',
            'date_entered' => 'Date Entered',
            'date_modified' => 'Date Modified',
            'source_id' => 'Source',
            'check_number' => 'Check Number',
            'method_id' => 'Method',
            'category_id' => 'Category',
            'custom_tag_id' => 'Custom Tag',
            'amount' => 'Amount',
            'split' => 'Split',
            'master_id' => 'Master ID',
            'inventory' => 'Inventory',
            'auto' => 'Auto',
            'auto_id' => 'Auto',
            'memo' => 'Memo',
            'reconciled' => 'Reconciled',
            'reconciled_date' => 'Reconciled Date',
            'locked' => 'Locked',
            'locked_date' => 'Locked Date',
            'approved' => 'Approved',
            'approved_date' => 'Approved Date',
            'accountant_changed' => 'Accountant Changed',
            'ref_source' => 'Ref Source',
            'ref_type' => 'Ref Type',
            'ref_code' => 'Ref Code',
        ];
    }

    public function afterFind()
    {
        $this->date = $this->toAppDate($this->date);

        parent::afterFind();
    }

    public function beforeValidate()
    {
        $this->date = $this->toStorageDate($this->date);

        return parent::beforeValidate();
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                if ($this->type == self::TYPE_BEGINNING_BALANCE) {
                    $this->category_id = 'e668e0f8b17acc983657c93131ea59b3';
                }
                if ($this->category_id == 'fa2f21f556984750e5954a9da01315b7') {
                    $this->auto ='Y';
                }
            }

            return true;
        }

        return false;
    }

    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) {
            if ($this->type == self::TYPE_BEGINNING_BALANCE) {
                $bankAccount = $this->getBankAccount()
                    ->one();
                $bankAccount->begin_balance = $this->amount;
                $bankAccount->balance = BankAccount::getAccountBalance($this->account_id);
                $bankAccount->save();
            }
        }
        parent::afterSave($insert, $changedAttributes);
    }

    public static function getTypes($all = false)
    {
        if ($all) {
            $result[self::TYPE_BEGINNING_BALANCE] = 'Beginning Balance';
        }
        else {
            $result = [
                self::TYPE_FUNDS_IN => 'Funds In',
                self::TYPE_FUNDS_OUT => 'Funds Out',
            ];

        }

        return $result;
    }

    public static function getMethods($all = false)
    {
        $result = [
            self::METHOD_CASH => self::METHOD_CASH,
            self::METHOD_CHECK => self::METHOD_CHECK,
            self::METHOD_CREDIT_CARD => self::METHOD_CREDIT_CARD,
            self::METHOD_DEBIT_CARD => self::METHOD_DEBIT_CARD,
            self::METHOD_DEPOSIT => self::METHOD_DEPOSIT,
            self::METHOD_ELECTRONIC => self::METHOD_ELECTRONIC,
            self::METHOD_RETURN_REFUND => self::METHOD_RETURN_REFUND,
        ];
        if ($all) {
            $result[self::METHOD_BEGINNING_BALANCE] = self::METHOD_BEGINNING_BALANCE;
        }

        return $result;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBankAccount()
    {
        return $this->hasOne(BankAccount::className(), ['id' => 'account_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSource()
    {
        return $this->hasOne(Source::className(), ['id' => 'source_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomTag()
    {
        return $this->hasOne(CustomTag::className(), ['id' => 'custom_tag_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImportedItem()
    {
        return $this->hasOne(ImportedItem::className(), ['id' => 'transaction_id']);
    }

    public static function getInfoWidgetData($year = false)
    {
        $userDB = Yii::$app->user->id;

        $dateStart = (!$year)?Helper::getCurrentYearDate():$year;

        $sql = 'SELECT sum(amount) as amounts,type FROM '.$userDB.'.TRANSACTION t  INNER JOIN '.$userDB.'.BANK_ACCOUNTS ba ON t.account_id =ba.id '.
        ' WHERE   amount>0 and master_id = \'\'  and  ba.acct_type=\'B\' or ba.acct_type=\'C\' '.
        ' AND t.date>=\'' . $dateStart . '\' and t.date< DATE_ADD(\'' . $dateStart .'\' , INTERVAL 1 YEAR)  '.
        '  GROUP BY type ';

        $results = Yii::$app->db->createCommand($sql)->queryAll();

        foreach ($results as $result) {
            $data[$result["type"]] =$result["amounts"];
        }

        $sql = 'SELECT sum(amount) as amounts,type FROM '.$userDB.'.TRANSACTION t  INNER JOIN '.$userDB.'.BANK_ACCOUNTS ba ON t.account_id =ba.id '.
            ' WHERE   amount<0 and master_id = \'\'  and  ba.acct_type=\'B\' or ba.acct_type=\'C\' '.
            ' AND t.date>=\'' . $dateStart . '\' and t.date< DATE_ADD(\'' . $dateStart .'\' , INTERVAL 1 YEAR)  '.
            '  GROUP BY type ';

        $results = Yii::$app->db->createCommand($sql)->queryAll();

        foreach ($results as $result) {
            $data[$result["type"]."2"] = $result["amounts"];
        }


        $funds_in = isset($data["I"])?$data["I"]:0 + isset($data["B"])?$data["B"]:0;
        $funds_out = isset($data["E"])?$data["E"]:0 - isset($data["A"])?$data["A"]:0 - isset($data["A2"])?$data["A2"]:0 - isset($data["I2"])?$data["I2"]:0 - isset($data["B2"])?$data["B2"]:0;
        $total = $funds_in - $funds_out;

        $res["funds_in_out"] = [
            'funds_in' => $funds_in,
            'funds_out' => $funds_out,
            'total' => $total
        ];



        $sql = 'SELECT  t.type as t_type, c.type as c_type, sum(t.amount ) as amounts '.
        'from '.$userDB.'.TRANSACTION t INNER JOIN '. Helper::getAppDb() .'.CATEGORY c ON t.category_id=c.id '.
        'where category_id != \'\' and category_id != \'split\' '.
        ' AND t.date>=\'' . $dateStart . '\' and t.date<DATE_ADD(\'' . $dateStart .'\' , INTERVAL 1 YEAR) '.
        'GROUP BY t.type, c.type';

        $results = Yii::$app->db->createCommand($sql)->queryAll();

        foreach ($results as $result) {
            $data[$result["t_type"]][$result["c_type"]] = $result["amounts"];
        }

        $income = isset($data["I"]["I"])?$data["I"]["I"]:0 - isset($data["E"]["I"])?$data["E"]["I"]:0;
        $expense = isset($data["E"]["E"])?$data["E"]["E"]:0 - isset($data["I"]["E"])?$data["I"]["E"]:0;
        $net_income = $income - $expense;

        $res["net_income"] = [
            'income' => $income,
            'expense' => $expense,
            'net_income' => $net_income
        ];


        return $res;
    }

    public static function getLast($year = null) {
        $query = self::find();

        if (!is_null($year)) {
            $query->where('date>=:year',[':year' =>$year]);
            $query->andWhere('DATE_ADD(:year,INTERVAL 1 YEAR)>date',[':year' =>$year]);
        }
        return $query->max('date');
    }
}
