<?php

namespace common\models\frontend;

use Yii;

/**
 * This is the model class for table "{{%OPP_TODO}}".
 *
 * @property string $id
 * @property string $opp
 * @property string $action
 * @property string $name
 * @property string $date
 * @property resource $data
 */
class OpportunityTodo extends \common\components\AppActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%OPP_TODO}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'data'
                ],
                'required'
            ],
            [
                ['date'],
                'safe'
            ],
            [
                ['data'],
                'string'
            ],
            [
                [
                    'id',
                    'opp',
                    'action'
                ],
                'string',
                'max' => 32
            ],
            [
                ['name'],
                'string',
                'max' => 255
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'opp' => 'Opp',
            'action' => 'Action',
            'name' => 'Name',
            'date' => 'Date',
            'data' => 'Data',
        ];
    }
}
