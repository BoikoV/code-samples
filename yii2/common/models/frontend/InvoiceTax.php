<?php

namespace common\models\frontend;

use Yii;

/**
 * This is the model class for table "{{%INVOICE_TAXES}}".
 *
 * @property string $id
 * @property string $invoice_id
 * @property string $taxcode_id
 * @property string $name
 * @property double $percent
 */
class InvoiceTax extends \common\components\AppActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%INVOICE_TAXES}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['id'],
                'required'
            ],
            [
                ['percent'],
                'number'
            ],
            [
                [
                    'id',
                    'invoice_id',
                    'taxcode_id'
                ],
                'string',
                'max' => 32
            ],
            [
                ['name'],
                'string',
                'max' => 60
            ]
            /*,
            [
                ['invoice_id'],
                'unique'
            ],
            */
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'invoice_id' => 'Invoice ID',
            'taxcode_id' => 'Taxcode ID',
            'name' => 'Name',
            'percent' => 'Percent',
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(Invoice::className(), ['id' => 'invoice_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTax()
    {
        return $this->hasOne(TaxTable::className(), ['id' => 'taxcode_id']);
    }
}
