<?php

namespace common\models\frontend;

use common\models\backend\Category;
use common\models\frontend\Transaction;
use Yii;

/**
 * This is the model class for table "{{%SOURCE}}".
 *
 * @property string $id
 * @property string $date_entered
 * @property string $name
 * @property string $category_id
 * @property string $cust_tag_id
 * @property string $memo
 * @property string $address_id
 * @property string $billing_id
 * @property string $auto
 * @property string $active
 *
 * @property Transaction[] $transactions
 * @property address $address
 */
class Source extends \common\components\AppActiveRecord
{
    const STATUS_ACTIVE = 'Y';
    public $group;



    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%SOURCE}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'name',
                ],
                'required'
            ],
            [
                ['date_entered'],
                'safe'
            ],
            [
                [
                    'id',
                    'category_id',
                    'cust_tag_id',
                    'address_id',
                    'billing_id'
                ],
                'string',
                'max' => 32
            ],
            [
                [
                    'name',
                    'memo'
                ],
                'string',
                'max' => 255
            ],
            [
                [
                    'auto',
                    'active'
                ],
                'string',
                'max' => 1
            ],
            [
                ['name'],
                'unique'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_entered' => 'Date Entered',
            'name' => 'Name',
            'category_id' => 'Category',
            'cust_tag_id' => 'Custom Tag',
            'memo' => 'Memo',
            'address_id' => 'Address',
            'billing_id' => 'Billing',
            'auto' => 'Auto',
            'active' => 'Active',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBankAccount()
    {
        return $this->hasOne(BankAccount::className(), ['id' => 'account_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransaction()
    {
        return $this->hasMany(Transaction::className(), ['source_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasMany(Invoice::className(), ['source_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddress()
    {
        return $this->hasOne(Address::className(), ['id' => 'address_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomtag()
    {
        return $this->hasOne(CustomTag::className(), ['id' => 'cust_tag_id']);
    }

    public static function getAll($showInactive = false, $beginBalance = false)
    {
        $query = self::find();
        if (!$showInactive) {
            $query->where(['active' => self::STATUS_ACTIVE]);
        }
        if ($beginBalance) {
            $result[0] = ['id'=>Transaction::METHOD_BEGINNING_BALANCE, 'name' =>Transaction::METHOD_BEGINNING_BALANCE];
        }
        else {
            $result = $query->all();
        }

        return $result;
    }

    public static function getInvoiceCustomer()
    {
        $query = self::find()
            ->select([
                'SOURCE.id',
                'SOURCE.name',
                ' CASE WHEN not isnull(source_id) then \'Invoiced customer\' else \'Other\' end as \'group\' ',
            ])

            ->joinWith('invoice')
            ->distinct();

        //echo $query->createCommand()->sql;
        //die();
        return $query->all();
    }

}
