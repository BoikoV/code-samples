<?php

namespace common\models\frontend;

use Yii;

/**
 * This is the model class for table "{{%TRANSFERS}}".
 *
 * @property string $from_id
 * @property string $to_id
 * @property string $type
 */
class Transfer extends \common\components\AppActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%TRANSFERS}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'from_id',
                    'to_id',
                    'type'
                ],
                'required'
            ],
            [
                [
                    'from_id',
                    'to_id'
                ],
                'string',
                'max' => 32
            ],
            [
                ['type'],
                'string',
                'max' => 1
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'from_id' => 'From ID',
            'to_id' => 'To ID',
            'type' => 'Type',
        ];
    }
}
