<?php

namespace common\models\backend;

use Yii;

/**
 * This is the model class for table "{{%PACKAGES}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $reg_code
 * @property integer $promotion_id
 * @property integer $active
 * @property integer $minimum_initial_recurrence
 */
class Package extends \common\components\ActiveRecord
{
    const DEFAULT_PACKAGE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%PACKAGES}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['description'],
                'required'
            ],
            [
                ['description'],
                'string'
            ],
            [
                [
                    'promotion_id',
                    'active',
                    'minimum_initial_recurrence'
                ],
                'integer'
            ],
            [
                ['name'],
                'string',
                'max' => 50
            ],
            [
                ['reg_code'],
                'string',
                'max' => 25
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'reg_code' => 'Reg Code',
            'promotion_id' => 'Promotion ID',
            'active' => 'Active',
            'minimum_initial_recurrence' => 'Minimum Initial Recurrence',
        ];
    }

    /**
     * @return null|static
     */
    public static function getDefaultPackage()
    {
        return self::findOne(['id' => self::DEFAULT_PACKAGE]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServices()
    {
        return $this->hasMany(Service::className(), ['id' => 'service_id'])
            ->viaTable(PackageService::tableName(), ['package_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscriptions()
    {
        return $this->hasMany(Subscription::className(), ['id' => 'subscription_id'])
            ->viaTable(PackageSubscription::tableName(), ['package_id' => 'id']);
    }
}
