<?php

namespace common\models\backend;

use Yii;

/**
 * This is the model class for table "AFFILIATES_SBR".
 *
 * @property string $affiliate_id
 * @property string $user_id
 * @property string $type
 * @property string $signup_date
 * @property string $active
 * @property string $invite_date
 * @property string $reg_code
 * @property resource $address
 * @property string $source
 */
class AFFILIATESSBR extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'AFFILIATES_SBR';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['affiliate_id', 'address'], 'required'],
            [['signup_date', 'invite_date'], 'safe'],
            [['address'], 'string'],
            [['affiliate_id', 'user_id', 'reg_code', 'source'], 'string', 'max' => 32],
            [['type'], 'string', 'max' => 3],
            [['active'], 'string', 'max' => 1],
            [['reg_code'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'affiliate_id' => 'Affiliate ID',
            'user_id' => 'User ID',
            'type' => 'Type',
            'signup_date' => 'Signup Date',
            'active' => 'Active',
            'invite_date' => 'Invite Date',
            'reg_code' => 'Reg Code',
            'address' => 'Address',
            'source' => 'Source',
        ];
    }
}


