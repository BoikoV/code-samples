<?php

namespace common\models\backend;

use Yii;

/**
 * This is the model class for table "AFFILIATE_CODES_SBR".
 *
 * @property string $affiliate_id
 * @property string $code
 * @property string $active
 * @property string $start_date
 * @property string $end_date
 * @property string $desc
 * @property string $type
 * @property resource $reg_php
 * @property resource $aff_php
 * @property double $license_discount
 * @property double $monthly_discount
 * @property integer $months_included
 */
class AFFILIATECODESSBR extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'AFFILIATE_CODES_SBR';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code'], 'required'],
            [['start_date', 'end_date'], 'safe'],
            [['reg_php', 'aff_php'], 'string'],
            [['license_discount', 'monthly_discount'], 'number'],
            [['months_included'], 'integer'],
            [['affiliate_id', 'code'], 'string', 'max' => 32],
            [['active', 'type'], 'string', 'max' => 1],
            [['desc'], 'string', 'max' => 255],
            [['code'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'affiliate_id' => 'Affiliate ID',
            'code' => 'Code',
            'active' => 'Active',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'desc' => 'Desc',
            'type' => 'Type',
            'reg_php' => 'Reg Php',
            'aff_php' => 'Aff Php',
            'license_discount' => 'License Discount',
            'monthly_discount' => 'Monthly Discount',
            'months_included' => 'Months Included',
        ];
    }
}
