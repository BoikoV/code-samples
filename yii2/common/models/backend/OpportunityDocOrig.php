<?php

namespace common\models\backend;

use Yii;

/**
 * This is the model class for table "OPP_DOC_ORIG".
 *
 * @property string $id
 * @property string $flow
 * @property string $name
 * @property string $subject
 * @property string $file
 * @property string $email_script
 * @property resource $letter_script
 */
class OpportunityDocOrig extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'OPP_DOC_ORIG';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['email_script', 'letter_script'], 'string'],
            [['id', 'flow', 'file'], 'string', 'max' => 32],
            [['name'], 'string', 'max' => 50],
            [['subject'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'flow' => 'Flow',
            'name' => 'Name',
            'subject' => 'Subject',
            'file' => 'File',
            'email_script' => 'Email Script',
            'letter_script' => 'Letter Script',
        ];
    }
}