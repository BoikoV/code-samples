<?php

namespace common\models\backend;

use Yii;

/**
 * This is the model class for table "{{%SERVICES}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property double $base_price
 * @property integer $active
 * @property string $execute
 * @property integer $execute_order
 * @property integer $trial_period
 * @property integer $full_service_id
 */
class Service extends \common\components\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%SERVICES}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['description'],
                'required'
            ],
            [
                ['description'],
                'string'
            ],
            [
                ['base_price'],
                'number'
            ],
            [
                [
                    'active',
                    'execute_order',
                    'trial_period',
                    'full_service_id'
                ],
                'integer'
            ],
            [
                ['name'],
                'string',
                'max' => 50
            ],
            [
                ['execute'],
                'string',
                'max' => 32
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'base_price' => 'Base Price',
            'active' => 'Active',
            'execute' => 'Execute',
            'execute_order' => 'Execute Order',
            'trial_period' => 'Trial Period',
            'full_service_id' => 'Full Service ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPackage()
    {
        return $this->hasOne(Package::className(), ['id' => 'package_id'])
            ->viaTable(PackageService::tableName(), ['service_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasMany(Order::className(), ['id' => 'order_id'])
            ->viaTable(Service::tableName(), ['service_id' => 'id']);
    }
}
