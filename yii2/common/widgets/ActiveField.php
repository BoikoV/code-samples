<?php

namespace common\widgets;
use yii\helpers\Html;

/**
 * @inheritdoc
 */
class ActiveField extends \yii\bootstrap\ActiveField
{
    /**
     * @param string $value
     * @return ActiveField $this
     */
    public function defaultValue($value)
    {
        if ($this->model->isNewRecord && !$this->model->{$this->attribute}) {
            $this->model->{$this->attribute} = $value;
        }

        return $this;
    }
    public function hiddenInput($options = [])
    {
        $options = array_merge($this->inputOptions, $options);
        $this->template = '{input}';
        $this->adjustLabelFor($options);
        $this->parts['{input}'] = Html::activeHiddenInput($this->model, $this->attribute, $options);

        return $this;
    }
}
