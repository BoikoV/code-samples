<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\models\backend\UserQuery */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'username',
            'address.first_name',
            'address.last_name',
            'active',
            'last_access',
            'nanny_status',
            'acct_type',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {messages}',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        $options = [
                            'title' => 'Details',
                            'aria-label' => 'Details',
                            'target' => '_blank'
                        ];

                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, $options);
                    },
                    'messages' => function ($url, $model, $key) {
                        $options = [
                            'title' => 'Messages',
                            'aria-label' => 'Messages',
                            'target' => '_blank'
                        ];

                        return Html::a('<span class="glyphicon glyphicon-envelope"></span>', $url, $options);
                    },
                ]
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
