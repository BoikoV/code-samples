<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'System Requirements';
?>
<h1>System Requirements</h1>
<div class="learn-more">
    <div class="tab-button">
        <?php echo Html::a( "back", ['site/learnmore'],  ["title"=>"back","class"=>"back"]); ?>
        <?php echo Html::a( "System Requirements", ['site/requirements'],  ["title"=>"System Requirements","class"=>"active"]); ?>
        <?php echo Html::a( "Data Security", ['site/security'],  ["title"=>"Data Security"]); ?>

    </div>
    <div class="aset">
        <h3>Keeping Your Data Safe & Secure</h3>
        <br />
        <h3>Using Microsoft Windows 2000, XP or greater</h3>
        <p>All the software you need is probably already on your computer. You will need: </p>
        <ul>
            <li>An internet connection (56Kbps or greater) </li>
            <li>A compatible internet browser:
                <ul>
                    <li>Mozilla Firefox v1.0 or greater <a class="link-style" href="http://www.getfirefox.com" target="_new">Get it here</a></li>
                    <li>Microsoft Internet Explorer v6.0 or greater or </li>
                </ul>
            </li>
            <li>Macromedia's Free Flash Player <a class='link-style' href="http://www.macromedia.com/go/getflashplayer/" target="_new">Get it here</a></li>
            <li>PDF Reader such as the free Adobe Acrobat Reader <a class='link-style' target="_new" href="http://www.adobe.com/products/acrobat/readstep2.html">Get it here</a></li>
        </ul>
        <hr>
        <h3>Using Mac OS X or greater</h3>
        <p>All the software you need is probably already on your computer. You will need: </p>
        <ul>
            <li>An internet connection (56Kbps or greater) </li>
            <li>Mozilla FireFox v1.0 or greater <a class='link-style' target="_new" href="http://www.getfirefox.com" target="_new">Get it here</a></li>
            <li>Macromedia's Free Flash Player <a class='link-style' target="_new" href="http://www.macromedia.com/go/getflashplayer/">Get it here</a></li>
            <li>PDF Reader such as the free Adobe Acrobat Reader <a class='link-style' target="_new" href="http://www.adobe.com/products/acrobat/readstep2.html">Get it here</a></li>
        </ul>
        <hr>
        <h3>Using Linux i686</h3>
        <p>Your computer will need to have the following:</p>
        <ul>
            <li>An internet connection (56Kbps or greater) </li>
            <li>Mozilla FireFox v1.0 or greater <a class='link-style' href="http://www.getfirefox.com" target="_new">Get it here</a></li>
            <li>Macromedia's Free Flash Player <a class='link-style' target="_new" href="http://www.macromedia.com/go/getflashplayer/">Get it here</a></li>
            <li>PDF Reader such as the free Adobe Acrobat Reader <a class='link-style' target="_new" href="http://www.adobe.com/products/acrobat/readstep2.html">Get it here</a></li>
        </ul>
        <hr>
        <h3>Browser requirements</h3>
        <p>Your browser will have these options enabled by default: </p>
        <ul>
            <li>JavaScript enabled </li>
            <li>SSL 128 bit encryption </li>
        </ul>
    </div>
</div>

    </div>