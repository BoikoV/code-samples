<div class="registration" >
    <div class="row text-center" >
        <h1>Create Your KeepMore.net Account</h1>
    </div>
    <form class="form-inline " role="form" id="regform">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-right">
                <div class="form-group">
                    <label for="billing_email">Email:</label>
                    <input type="email" class="form-control validate-email" id="billing_email" name="billing_email" required="required">
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 xs-12 text-left">
                <div class="form-group">
                    <label for="password1">Password:</label>
                    <input type="password" class="form-control" id="password1" name="password1" required="required" >
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 xs-12 text-right">
                <div class="form-group">
                    <label for="password2">Retype Password:</label>
                    <input type="password" class="form-control" id="password2" name="password2" required="required" >
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 xs-12 text-left">
                <div class="form-group">
                    <label for="billing_company">Company:</label>
                    <input type="text" class="form-control" id="billing_company" name="billing_company" >
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 xs-12 text-right">
                <div class="form-group">
                    <label for="billing_firstname">First Name: </label>
                    <input type="text" class="form-control" id="billing_firstname" name="billing_firstname" required="required" >
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 xs-12 text-left">
                <div class="form-group">
                    <label for="billing_initial">Middle Initial: </label>
                    <input type="text" class="form-control" id="billing_initial" name="billing_initial"  >
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 xs-12 text-right">
                <div class="form-group">
                    <label for="billing_lastname">Last Name: </label>
                    <input type="text" class="form-control" id="billing_lastname" name="billing_lastname" required="required" >
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 xs-12 text-left">
                <div class="form-group">
                    <label for="billing_address1">Address 1: </label>
                    <input type="text" class="form-control" id="billing_address1" name="billing_address1" required="required" >
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 xs-12 text-right">
                <div class="form-group">
                    <label for="billing_address2">Address 2: </label>
                    <input type="text" class="form-control" id="billing_address2" name="billing_address2"  >
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 xs-12 text-left">
                <div class="form-group">
                    <label for="billing_zip">Zipcode: </label>
                    <input type="text" class="form-control" id="billing_zip" name="billing_zip" required="required" >
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 xs-12 text-right">
                <div class="form-group">
                    <label for="billing_city">City:</label>
                    <input type="text" class="form-control" id="billing_city" name="billing_city" required="required" >
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 xs-12 text-left">
                <div class="form-group">
                    <label for="billing_state">State: </label>
                    <input type="text" class="form-control" id="billing_state" name="billing_state" required="required" >
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 xs-12 text-right">
                <div class="form-group">
                    <label for="billing_phone">Phone:</label>
                    <input type="text" class="form-control" id="billing_phone" name="billing_phone" required="required" >
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 xs-12 text-left">
                <div class="form-group">
                    <label for="billing_mobile">Cell: </label>
                    <input type="text" class="form-control" id="billing_mobile" name="billing_mobile" required="required" >
                </div>
            </div>
        </div>
        <div class="row text-center">
            <h2>Enter Your Billing Information</h2>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 xs-12 text-right">
                <div class="form-group">
                    <label for="billing_cardtype">Card Type:</label>
                    <select class="form-control"  id="billing_cardtype" name="billing_cardtype">
                        <option value="Visa">Visa</option>
                        <option value="MasterCard">MasterCard</option>
                        <option value="AMEX">American Express</option>
                    </select>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 xs-12 text-left">
                <div class="form-group">
                    <label for="billing_mobile">Card Number: </label>
                    <input type="text" class="form-control" id="billing_cardnumber" name="billing_cardnumber" required="required" placeholder="**** **** **** ****" >
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 xs-12 text-right">
                <div class="form-group">
                    <label for="billing_cardexp">Expiration:</label>
                    <input type="text" class="form-control" id="billing_cardexp" name="billing_cardexp" required="required" >
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 xs-12 text-left">
                <div class="form-group">
                    <label for="billing_mobile">CVV Number: </label>
                    <input type="password" class="form-control" id="billing_cardnumber" name="billing_cardnumber" required="required" placeholder="***" >
                </div>
            </div>
        </div>
        <div class="row text-center">
            <h2>Choose your payment recurrence.</h2>
        </div>
        <div class="row text-center">
            <div class="form-group " >
                <div class="radio">
                    <label>
                        <input type="radio" name="subscription_id" id="subscription_id" value="1" checked>
                        Monthly
                    </label>
                </div>
                <div class="radio ">
                    <label>
                        <input type="radio" name="subscription_id" id="subscription_id" value="3">
                        Yearly
                    </label>
                </div>
            </div>
        </div>
        <div class="row text-center">
            <h2>Your Services</h2>
        </div>
        <div class="row text-center">
            <div class="col-sm-offset-3 col-lg-offset-3 col-md-offset-3 col-lg-6 col-md-6 col-sm-6 xs-12 ">
                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <td ><img src="/images/tick.png"> KeepMore.net</td><td>$19.95</td>
                        </tr>
                        <tr>
                            <td ><img src="/images/tick.png"> eDocument Storage +1GB</td><td>$19.95</td>
                        </tr>
                        <tr>
                            <td ><img src="/images/tick.png"> Income/Expense Tracking (1000 Transactions)</td><td>$19.95</td>
                        </tr>
                        <tr>
                            <td ><img src="/images/tick.png"> Outgoing Emails (1000 Emails)</td><td>FREE</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="row text-center">
            <h2>Comment Additional Information</h2>
        </div>
        <div class="row text-center">
            <div class="col-sm-offset-3 col-lg-offset-3 col-md-offset-3 col-lg-6 col-md-6 col-sm-6 xs-12 ">
               <textarea class="form-control input-medium" rows="5" id="comment" ></textarea>
            </div>
        </div>
        <div class="row text-center">
            <span class="save-btn">
                <input type="submit"  class="button" value="Create Account">
            </span>
            <span class="cancel-btn">
                <input type="submit" href="javascript:void(0)" class="button" value="Cancel">
            </span>
        </div>

    </form>
 </div>