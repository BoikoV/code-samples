<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Live Demo';
?>
<h1>Live Demo</h1>
<div class="container-left">
    <h2 class="style2">You're about to try out KeepMore, using our live demo.</h2>

    <div class="live-demo">
        <p>Here's how it's going to work. You will be entering information for your fake company, and entering some expenses - and even some income. Then you'll be able to look at what you entered by choosing to 'view' your information, or by choosing a report. It's that easy. And if you aren't sure of something along the way, just click 'Guide Me' and you'll get some help.</p>
        <br>
        <p>Don't be surprised if you see other data than your own - other folks might also be trying out our Live Demo at the same time. So don't put in any of your real information!</p>

        <h3>A couple of quick hints. </h3>
        <div class="inner-bx">
            <ul>
                <li>You'll be clicking on some large picture icons, like 'Track Auto' or 'Generate Reports'. They are located in a bar at the top of the page. Then you just follow the directions and you are off and running.</li>
                <li>Opportunity Tracker - don't forget to try out OT - it's included with your KeepMore account. You can track your vendors, inventory sources, leads and contacts. And you can get reminder follow-up emails to keep in touch with all of them. Just go to Premium Services, or look for the OT logo under 'Enter Transactions'.</li>
                <li>eDocument storage - try this out as well! It's found under Premium Services.</li>
            </ul>
        </div>

        <h3>Enjoy!</h3>
        <div class="blue-strip"><a href="Launch Demo" onClick="window.open('/app/demo_login.html','demo','width=800,height=650,menubar=yes,resizable=yes,scrollbars=yes'); return false;">Click Here to Launch the KeepMore Live Demo</a></div>
    </div>
</div>
<div class="container-right" id="contentPromoRight">
    <?= $this->render( 'right_banners'); ?>
</div>
