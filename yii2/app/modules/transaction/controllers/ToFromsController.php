<?php
namespace app\modules\transaction\controllers;

use Yii;
use common\models\frontend\Source;
use common\models\frontend\Address;
use app\modules\transaction\components\Controller;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

class ToFromsController extends Controller
{
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Source::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        return $this->_process(new Source(), new Address());
    }

    public function actionUpdate($id)
    {
        $modelSource = $this->findSourceModel($id);
        if (!($modelAddress = $modelSource->getAddress()
            ->one())) {
            $modelAddress = new Address();
        }
        return $this->_process($modelSource, $modelAddress);
    }

    protected function _process(Source $modelSource, Address $modelAddress)
    {
        $success = false;
        if ($modelSource->load(Yii::$app->request->post()) && $modelAddress->load(Yii::$app->request->post())) {
            if ($modelAddress->validate() && $modelSource->validate()) {
                $modelAddress->save(false);
                $modelSource->address_id = $modelAddress->id;
                $modelSource->save(false);
                $success = true;
            }
        }
        if (!$success) {
            return $this->renderAjax('_form', [
                'modelSource' => $modelSource,
                'modelAddress' => $modelAddress,
            ]);
        }
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Source the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findSourceModel($id)
    {
        if (($model = Source::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
