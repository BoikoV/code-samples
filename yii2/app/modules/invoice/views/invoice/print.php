<?php
    use common\models\frontend\Invoice;
    //use Number;
?>
<body>
<div style="float: left; width:100%">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="80%" class="row-black">
        </td>
        <td class="report_type" align="center"><i>INVOICE</i></td>
    </tr>
    <tr class="block_enter"><td colspan="2" class="block_enter"></td></tr>
    <tr>
        <td colspan="2">
            <table width="100%">
                <tr>
                    <td width="60%">
                        <table width="100%" class="table-bordered" cellspacing="0" cellpadding="0">
                            <tr class="row-gray"><td align="center"><b>From</b></td></tr>
                            <tr><td><b><?=$order->billing_company; ?></b></td></tr>
                            <tr><td><?=$order->billing_address1; ?></td></tr>
                            <tr><td><?=$order->billing_city; ?>, <?=$order->billing_state; ?></td></tr>
                            <tr><td><?=$order->billing_zip; ?></td></tr>
                            <tr><td><?=$order->billing_phone; ?></td></tr>
                        </table>
                    </td>
                    <td width="2%"></td>
                    <td valign="bottom">
                        <table width="100%" class="table-bordered" cellspacing="0" cellpadding="0">
                            <tr >
                                <td colspan="2" align="center" style="border-bottom: 1px solid #000;">
                                    <?php
                                    if ($logo) {
                                        $img = $logo->getFileData()
                                            ->one()
                                            ->filedata;
                                        echo '<img height=50px src="data:image/jpeg;base64,'.base64_encode(stripslashes($img)).'" />';
                                    }
                                    ?>
                                    </td>
                            </tr>
                            <tr class="row-gray">
                                <td align="center" class="dotted-black bold">Date</td>
                                <td align="center" class="dotted-black bold">Invoice #</td>
                            </tr>
                            <tr>
                                <td align="center" class="dotted-black text-12 bold"><?=Yii::$app->formatter->asDate($invoice->date, 'MM/dd/yyyy'); ?></td>
                                <td align="center" class="dotted-black text-12 bold"><?=$invoice->inv_number; ?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
             </table>
        </td>
    </tr>
    <tr class="block_enter"><td colspan="2" class="block_enter"></td></tr>
    <tr>
        <td colspan="2" height="150px" valign="top">
            <table width="100%" height="100%" >
                <tr>
                    <td width="49%" height="150px" valign="top">
                        <table height="150px" width="100%"  cellspacing="0" cellpadding="0" class="table-bordered"  >
                            <tr class="row-black">
                                <td align="center">Bill To:</td>
                            </tr>
                            <tr height="150px">
                                <td valign="top" height="120px">
                                    <?php
                                    $lines = explode("\n",$invoice->bill_addr);
                                    foreach($lines as $line){
                                    echo $line."<br>";
                                    }
                                    ?>&nbsp;
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td></td>
                    <td width="49%" height="100%" valign="top">
                        <table width="100%" cellspacing="0" cellpadding="0" class="table-bordered" height="100%" style="min-height: 200px;">
                            <tr class="row-black">
                                <td align="center">Ship To:</td>
                            </tr>
                            <tr height="120px">
                                <td valign="top" height="120px"><?php
                                    $lines = explode("\n",$invoice->ship_addr);
                                    foreach($lines as $line){
                                        echo $line."<br>";
                                    }
                                    ?>&nbsp;
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr class="block_enter"><td colspan="2" class="block_enter"></td></tr>
    <tr>
        <td colspan="2">
            <table width="100%" class="table-bordered" cellspacing="0" cellpadding="0">
                <tr>
                    <td rowspan="2" align="center" style="border-right:1px solid #000;background:lightgray;font-weight: bold;color:#000; font-size:20px;">Invoice<br>Detail</td>
                    <td align="center" class="header-gray dotted-white text-12" >Purchase Order #</td>
                    <td align="center" class="header-gray dotted-white text-12" >Terms</td>
                    <td align="center" class="header-gray text-12">Due Date</td>
                </tr>
                <tr >
                    <td align="center" class="cell-gray dotted-black text-12 bold" ><?=$invoice->po_number; ?></td>
                    <td align="center" class="cell-gray dotted-black text-12 bold" ><?=Invoice::getTerms($invoice->terms); ?></td>
                    <td align="center" class="cell-gray text-12 bold"><?=$invoice->due_date; ?></td>
                </tr>

            </table>

        </td>
    </tr>
    <tr class="block_enter"><td colspan="2" class="block_enter"></td></tr>
    <tr>
        <td colspan="2">
            <table width="100%" border="0" class="table-bordered table-stripped" cellspacing="0" cellpadding="0">
                <tr>
                    <th align="center" class="header-gray dotted-white text-12" >Qty</th>
                    <th align="center" class="header-gray dotted-white text-12">Item</th>
                    <th align="center" class="header-gray dotted-white text-12">Description</th>
                    <th align="center" class="header-gray dotted-white text-12">Price Each</th>
                    <th align="center" class="header-gray dotted-white text-12">Tax</th>
                    <th align="center" class="header-gray text-12">Extended Price</th>
                </tr>
                <?php foreach($invoiceItems  as $key => $item)
                {
                ?>
                <tr>
                    <td class="dotted-black text-12 border-down" align="center" width="10%"><?=$invoiceItems[$key]['qty'] ;?></td>
                    <td class="dotted-black text-12 border-down"><?=$invoiceItems[$key]['item_name'] ;?></td>
                    <td class="dotted-black text-12 border-down"><?=$invoiceItems[$key]['desc'] ;?></td>
                    <td class="dotted-black text-12 border-down" align="right" width="10%"><?=number_format($invoiceItems[$key]['price_each'],2, '.', ' ') ;?></td>
                    <td class="dotted-black text-12 border-down" align="center" width="10%">
                        <?php if ($invoiceItems[$key]['tax']==1) echo "T" ;?>
                    </td>
                    <td align="right" class="border-down text-12 " width="10%"><?=$invoiceItems[$key]['total_sum_no_tax'] ;?></td>
                </tr>
                <?php
                }
                ?>
             </table>
        </td>
    </tr>
    <tr class="block_enter"><td colspan="2" class="block_enter"></td></tr>
    <tr>
        <td colspan="2">
            <table width="100%" border="0" height="100%" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="53%" valign="top" height="140px" cellspacing="0" cellpadding="0">
                        <table height="120px" class="table-bordered" width="100%" cellspacing="0" cellpadding="0">
                            <tr class="row-black">
                                <td>Notes</td>
                            </tr>
                            <tr height="100px">
                                <td height="95px" class="border-down"><?=$invoice->notes;?>&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="row-gray text-12 bold">Powered by SageFire</td>
                            </tr>
                        </table>
                    </td>
                    <td width="2%"></td>
                    <td valign="top">
                        <table width="100%" class="table-bordered table-stripped" cellspacing="0" cellpadding="0">
                            <tr class="row-black">
                                <th colspan="2" align="left" class="text-14">Totals:</th>
                            </tr>
                            <tr >
                                <td align="right" class="dotted-black text-12 border-down bold">Invoice subtotal:</td>
                                <td align="right" class="text-13 border-down bold"><?=$invoice->total_sum_without_tax; ?></td>
                            </tr>
                            <tr >
                                <td align="right" class="dotted-black text-12 border-down bold">Sales Tax %:</td>
                                <td align="right" class="text-13 border-down bold"><?=number_format($invoice->tax_rate,2, '.', ' ');?></td>
                            </tr>
                            <tr >
                                <td align="right" class="dotted-black text-12 border-down bold">Sales Tax:</td>
                                <td align="right" class="text-13 border-down bold"><?php echo number_format($invoice->total_sum_tax,2, '.', ' ');?></td>
                            </tr>
                            <tr >
                                <td align="right" class="dotted-black text-12 border-down bold">Total Invoice:</td>
                                <td align="right" class="text-13 border-down bold"><?=$invoice->total_sum_with_tax;?></td>
                            </tr>
                            <tr>
                                <td align="right" class="dotted-black text-12 border-down bold">Payment Total:</td>
                                <td align="right" class="text-13 border-down bold"><?=$invoice->total_sum_payment;?></td>
                            </tr>
                            <tr class="row-black">
                                <th class="dotted-white text-12 border-down bold" align="left">Amount Due $:</td>
                                <th align="right" class="text-13 border-down bold"><?=number_format(round($invoice->total_sum_with_tax - $invoice->total_sum_payment,2),2, '.', ' ');?></td>
                            </tr>

                        </table>
                    </td>
                 </tr>
             </table>
        </td>
    </tr>

</table>
</div>
<?php
if ($invoice->status==3){
?>
    <div style=" font-weight:bold;color:darkred;font-size: 60px;top:430px; float:left; position: absolute;border:0px solid red; height:200px;width:300px;left:40%;">
        VOID
    </div>
<?php
}

?>
</body>