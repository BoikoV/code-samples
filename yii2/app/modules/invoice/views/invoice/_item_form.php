<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\frontend\InvoiceItem;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;

/* @var $invoiceLineItem common\models\frontend\InvoiceLineItem */
/* @var $form yii\widgets\ActiveForm */


?>

<div class="col-lg-12 text-center"><b>Invoice Items</b></div>
<div class="col-lg-12">
    <?php Pjax::begin(); ?>
    <?php $form = ActiveForm::begin([
        'options' => [
            'id' => 'invoice-item-form',
            'class' => 'form-horizontal',
            'data-pjax' => true,
        ],
        'fieldConfig' => [
            'template' => "<div class=\"col-md-3\">{label}</div>\n<div class=\"col-md-9\">{input}</div>\n<div class=\"col-md-12\">{error}</div>",
        ],
    ]); ?>

    <?= $form->field($invoiceLineItem, 'income_item_id')
        ->widget(Select2::classname(), [
            'data' => ArrayHelper::map(InvoiceItem::GetAll(), 'id', 'name'),
            'options' => ['placeholder' => 'Select item ', 'options' => InvoiceItem::getPrices()],
            'pluginEvents' => ['select2:select' =>
                'function() {
                    $("#invoicelineitem-price_each").val($(this).find(":selected").data("price"));
                    if ($(this).find(":selected").data("tax")=="Y"){ $("#invoicelineitem-tax").prop("checked",true); }
                    else
                    {
                        $("#invoicelineitem-tax").prop("checked",false);
                    }
                }'],
        ]); ?>
    <?= $form->field($invoiceLineItem, 'price_each')
        ->textInput(['maxlength' => true]) ?>

    <?= $form->field($invoiceLineItem, 'qty')
        ->textInput(['maxlength' => true]) ?>

    <?= $form->field($invoiceLineItem, 'desc')
        ->textInput(['maxlength' => true]) ?>

    <?= $form->field($invoiceLineItem, 'tax',
        ['template' => "<div class=\"col-md-3\">{label}</div>\n<div class=\"col-md-9\">{hint}\n{input}\n{beginLabel}{endLabel}</div>\n<div class=\"col-md-12\">{error}</div>",])
        ->checkbox([], false) ?>

    <div class="form-group modal-footer">
        <?= Html::submitButton('<span class="glyphicon glyphicon-ok-circle"></span> ' . ($invoiceLineItem->isNewRecord
                ? 'Add'
                : 'Update'), [
            'class' => 'btn btn-success'
        ]); ?>
        <button type="button" class="btn btn-success" data-dismiss="modal"><span
                class="glyphicon glyphicon-ban-circle"></span> Cancel
        </button>
    </div>
    <?php ActiveForm::end(); ?>
    <?php Pjax::end(); ?>
</div>
