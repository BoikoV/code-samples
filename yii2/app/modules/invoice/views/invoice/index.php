<?php

use yii\helpers\Html;
use common\components\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\ActiveForm;
use dosamigos\datepicker\DatePicker;
use yii\helpers\Url;
use common\models\frontend\Source;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\icons\Icon;
use common\models\frontend\Invoice;

/* @var $this yii\web\View */
/* @var $searchModel common\models\frontend\InvoiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Transactions';
?>
<div class="body-content-header">
    <div class='col-md-12'>
        <?php $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
            'options' => [
                'class' => 'form-horizontal'
            ],
            'fieldConfig' => [
                'options' => [
                    'tag' => false,
                ]
            ],
        ]); ?>
        <div class='row'>

            <div class="col-md-5">
                <div class="date">
                    <?= $form->field($searchModel, 'dateFrom', ['template' => '{label}{input}'])
                        ->widget(DatePicker::className(), ['addon' => ''])
                        ->label('Date Range') ?>
                </div>
                <div class="date">
                    <?= $form->field($searchModel, 'dateTo', ['template' => '{label}{input}'])
                        ->widget(DatePicker::className(), ['addon' => ''])
                        ->label('To') ?>
                </div>
            </div>
            <div class="col-md-4">
                <?= $form->field($searchModel, 'status',
                    ['template' => '<div>{label}<div class="col-md-10 ">{input}</div></div>'])
                    ->widget(Select2::classname(), [
                        'data' => $searchModel::getStatus(),
                        'options' => ['placeholder' => 'Select Status'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ]
                    ]); ?>
            </div>
            <div class="col-md-3">
                <?= Html::submitButton('Search', ['class' => ' btn btn-success']) ?>
                <?= Html::a('Reset', [''], ['class' => 'btn btn-success']) ?>
            </div>

        </div>
        <div class='row'>
            <div class="col-md-5">
                <?= $form->field($searchModel, 'source_id',
                    ['template' => '<div style="margin-left: 3px;">{label}<div class="col-md-10 ">{input}</div></div>'])
                    ->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(Source::getInvoiceCustomer(), 'id', 'name', 'group'),
                        'options' => ['placeholder' => 'Select Customer'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ]

                    ]); ?>
            </div>
            <div class="col-md-4"></div>

        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
<div class="body-content-options">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <button class="btn btn-success plus" id="btnAdd" data-url="<?= Url::to([
                        'create',
                    ]) ?>"><i
                            class="fa fa-plus" aria-hidden="true"> </i> Add New
                    </button>
                </div>
            </div>
            <div class="col-md-6 actions">

                <button title="Print" data-action="print" disabled="disabled" class="btn btn-success btn-action btn-other-action"
                        type="button">
                    <span class="action ">Print</span>
                </button>

                <button title="Void" data-action="void" disabled="disabled" class="btn btn-success btn-action btn-other-action"
                        type="button">
                    <span class="action ">Void</span>
                </button>

                <button title="Update" data-action="update" disabled="disabled" class="btn btn-action"
                        type="button">
                    <span class="action edit"></span>
                </button>

            </div>
        </div>
    </div>
</div>
<div>
    <?php Pjax::begin(['id' => 'invoice-pjax-grid', 'linkSelector' => 'a:not(.linksWithTarget)']); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'showFooter' => true,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'inv_date',
                'label' => 'Date',
                'format' => 'date',
                'contentOptions' => ['style' => 'width:80px;']
            ],
            [
                'label' => 'Number',
                'attribute' => 'inv_number',
                'contentOptions' => ['style' => 'width:90px;']
            ],
            [
                'attribute' => 'source.name',
                'label' => 'Customer',
                'contentOptions' => ['class' => 'text-left'],
            ],
            [
                'label' => 'Total',
                'attribute' => 'total_sum_with_tax',
                'contentOptions' => ['style' => 'width:90px;','class' => 'text-right']
            ],
            [
                'label' => 'Payment',
                'contentOptions' => ['style' => 'width:90px;','class' => 'text-right'],
                'value' => function ($model) {
                    return Invoice::getTotalSumPayment($model->id);
                }

            ],
            [
                'label' => 'Status',
                'value' => function ($model) {
                    $status=$model->status;
                    //return $status;
                    return $model->getStatus($status);
                }
            ],


            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Action',
                'template' => '{view} {payments}',
                'contentOptions' => ['style' => 'width:70px;'],
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a(Icon::show('newspaper-o', ['class' => 'btn-link']), Url::to([
                            'invoice/items',
                            'id' => $model->id,
                            'retURL' => Yii::$app->request->absoluteUrl
                        ]), [
                            'title' => 'Items',
                        ]);
                    },
                    'payments' => function ($url, $model) {
                        return Html::a(Icon::show('money'), Url::to([
                            'invoice/payments',
                            'id' => $model->id,
                            'retURL' => Yii::$app->request->absoluteUrl
                        ]), [
                            'title' => 'payments',
                        ]);
                    },
                ],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'display:none;'],
                'template' => '{update} {void} {print}',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::button('<span class="glyphicon glyphicon-pencil"></span>', [
                            'title' => 'Update',
                            'class' => 'btn btn-link update',
                            'data-action' => 'update',
                            'data-url' => Url::to([
                                'invoice/update',
                                'id' => $model->id
                            ])
                        ]);
                    },
                    'void' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', Url::to([
                            'invoice/void',
                            'id' => $model->id
                        ]), [
                            'data-action' => 'void',
                            'data-confirm' => Yii::t('yii', 'Are you sure that you want to void this item?'),
                            'data-pjax' => 'invoice-pjax-grid',
                        ]);
                    },
                    'print' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-print"></span>', Url::to([
                            'invoice/print',
                            'id' => $model->id
                        ]), [
                            'target'=>'_blank',
                            //'class' =>'linksWithTarget',
                            'data-action' => 'print',
                            //'data-confirm' => Yii::t('yii', 'Are you sure that you want to void this item?'),

                        ]);
                    },
                ]



            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>

<?= \app\components\PjaxModalForm::get([
    'modalId' => 'modal',
    'modalHeader' => 'Invoice',
    'selectorActivation' => '#btnAdd,[data-action="update"]',
    'formId' => 'invoice-form',
    'gridPjaxId' => 'invoice-pjax-grid'
]) ?>
