<?php
namespace app\modules\invoice\controllers;

use Yii;
use app\modules\invoice\components\Controller;

/**
 * FirstTimeController
 */
class FirstTimeController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }
}
