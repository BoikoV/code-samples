<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use dosamigos\datepicker\DatePicker;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use common\models\frontend\BankAccount;
use app\models\ReportFilterForm;

?>
<?php
$form = ActiveForm::begin([
    'action' => Url::to([$report]),
    'options' => ['target' => '_blank']


]);
?>
<div class="row">
    <div class="col-md-5">
        <div class="row">
            <?php
            echo $form->field($model, 'from',
                ['template' => '<div class="col-md-4">{label}</div><div class="col-md-6 no-margin">{input}</div>'])
                ->widget(DatePicker::className(), ['addon' => '']);
            ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-5">
        <div class="row">
            <?php
            echo $form->field($model, 'to',
                ['template' => '<div class="col-md-4">{label}</div><div class="col-md-6 no-margin">{input}</div>'])
                ->widget(DatePicker::className(), ['addon' => '']);
            ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-5">
        <div class="row">
            <?= $form->field($model, 'account',
                ['template' => '<div class="col-md-4">{label}</div><div class="col-md-6 no-margin">{input}</div>'])
                ->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(BankAccount::getLoansAll(), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'All',
                        'multiple' => true
                    ],
                ])->label('Loan'); ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-5">
        <div class="row">
            <?= $form->field($model, 'type_view',
                ['template' => '<div class="col-md-4">{label}</div><div class="col-md-6 no-margin">{input}</div>'])
                //->defaultValue($model::VIEW_HTML)
                ->label('View')
                ->radioList([
                    ReportFilterForm::VIEW_TYPE_DETAILED => 'Detailed',
                    ReportFilterForm::VIEW_TYPE_SHORT => 'Short',
                ], [
                    'item' => function ($index, $label, $name, $checked, $value) use ($model) {
                        $id = Html::getInputId($model, 'type_view') . '_' . $index;

                        return '<div class="col-md-6 labeled">' . Html::radio($name, $checked, [
                            'id' => $id,
                            'value' => $value
                        ]) . '<label for="' . $id . '"><span>' . $label . '</span></label></div>';
                    },
                    'value' => ReportFilterForm::VIEW_TYPE_DETAILED,
                ]) ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-5">
        <div class="row">
            <?= $form->field($model, 'output',
                ['template' => '<div class="col-md-4">{label}</div><div class="col-md-6 no-margin">{input}</div>'])
                //->defaultValue($model::VIEW_HTML)
                ->label('Output')
                ->radioList([
                    'html' => 'HTML',
                    'pdf' => 'PDF',
                ], [
                    'item' => function ($index, $label, $name, $checked, $value) use ($model) {
                        $id = Html::getInputId($model, 'output') . '_' . $index;

                        return '<div class="col-md-6 labeled">' . Html::radio($name, $checked, [
                            'id' => $id,
                            'value' => $value
                        ]) . '<label for="' . $id . '"><span>' . $label . '</span></label></div>';
                    },
                    'value' => ReportFilterForm::VIEW_HTML,
                ]) ?>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-5  text-center">
        <?php
        echo Html::submitButton('Generate report', [
            'class' => 'btn btn-success',
            'id' => 'btn_report',
        ]);
        $form::end();
        ?>
    </div>
</div>