<?php

namespace app\modules\service\controllers;

use Yii;
use app\modules\service\components\Controller;
use common\components\DocStorage;
use yii\helpers\Url;

/**
 * OpportunityController
 */
class DocStorageController extends Controller
{
    public function actionIndex()
    {
        if (!DocStorage::isEnabled()){
            return $this->redirect(Url::to(['/service']));
        }

        return $this->render('index');
    }
}
