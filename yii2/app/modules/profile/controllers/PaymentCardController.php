<?php
namespace app\modules\profile\controllers;

use Yii;
use common\models\backend\Order;
use common\components\Stripe;
use app\modules\profile\components\Controller;

/**
 * PaymentCardController
 */
class PaymentCardController extends Controller
{
    public function actionIndex()
    {
        $order = $this->findOrder();
        $customer = Stripe::getCustomer($order->stripe_customer);
        if (count($customer->sources->data) == 0) {
            return $this->redirect('/profile/payment-card/add');
        }

        return $this->render('index', [
            'order' => $order,
            'customer' => $customer
        ]);
    }

    public function actionAdd()
    {
        $model = $this->findOrder();
        $model->setScenario(Order::SCENARIO_ADD_CARD);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Stripe::AddCard($model->stripe_customer, $model->stripe_token);

            return $this->redirect('/profile/payment-card');
        }

        return $this->render('add', [
            'model' => $model,
        ]);
    }

    public function actionDelete()
    {
        $order = $this->findOrder();
        if (Stripe::DeleteCard($order->stripe_customer)) {
            $order->stripe_token = null;
            $order->save();
        }

        return $this->redirect('/profile/payment-card');
    }
}
