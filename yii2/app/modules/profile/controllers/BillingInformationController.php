<?php
namespace app\modules\profile\controllers;

use Yii;
use app\modules\profile\components\Controller;

/**
 * BillingInformationController
 */
class BillingInformationController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index', [
        ]);
    }
}
