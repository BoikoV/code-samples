<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\backend\Message;
use yii\widgets\Pjax;
use common\components\GridView;

/* @var $this yii\web\View */
/* @var common\models\frontend\AutoMileage */
/* @var $form yii\widgets\ActiveForm */


?>
<div class="col-md-2 col-lg-2 text-right"><b>Sent date:</b></div><div class="col-md-4 col-lg-4"><?=$model->date_sent;?></div>
<div class="col-md-2 col-lg-2 text-right"><b>Viewed date:</b></div><div class="col-md-4 col-lg-4"><?=$model->date_read;?>&nbsp;</div>
<div class="col-md-2 col-lg-2 text-right"><b>To:</b></div><div class="col-md-4 col-lg-4"><?=$model->getTo()->one()->username;?></div>
<div class="col-md-2 col-lg-2 text-right"><b>Subject:</b></div><div class="col-md-4 col-lg-4"><?=$model->subject;?></div>
<div class="col-md-2 col-lg-2 text-right"><b>Message:</b></div><div class="col-md-10 col-lg-10"><?=$model->message;?></div>

<div class="row text-center"><b>Message history</b></div>
<div class="col-lg-12">
    <?php Pjax::begin(['id' => 'answer-pjax-grid']) ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'id' => 'table-answer',
        'showFooter' => true,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Sent',
                'value' => function ($model) {
                    return $model->date_sent;
                },
                'headerOptions'=>array('width'=>'110px;'),
            ],
            [
                'label' => 'Sender',
                'value' => 'creator.username',
                'contentOptions'=>array('class'=>'text-left'),
            ],
            [
                'label' => 'Message',
                'value' => function ($model) {
                    return $model->message;
                },
                'contentOptions'=>array('class'=>'text-left'),
                'headerOptions'=>array('class'=>'text-left'),

            ],
            [
                'attribute' => 'date_read',
                'contentOptions' => ['style' => 'display:none;']
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
    </div>


<div class="col-lg-12 text-center"><b>Your answer</b></div>
<div class="col-lg-12">

<?php Pjax::begin(); ?>
<?php $form = ActiveForm::begin([
    'options' => [
        'id' => 'answer-form',
        'class' => 'form-horizontal',
        'data-pjax' => true,
    ],
    'fieldConfig' => [
        'template' => "<div class=\"col-md-3\">{label}</div>\n<div class=\"col-md-9\">{input}</div>\n<div class=\"col-md-12\">{error}</div>",
    ],
]); ?>


<?= $form->field($answer, 'message')
    ->textarea();?>


<?= $form->field($answer, 'from_id',['template'=>'{input}'])
    ->hiddenInput(['value'=>Yii::$app->session->get('__id')])->label(false); ?>

<?= $form->field($answer, 'thread_id')
    ->hiddenInput(['value'=>$model->id ])->label(false); ?>

<?= $form->field($answer, 'to_id')
    ->hiddenInput(['value'=>$model->to_id ])->label(false); ?>





<div class="form-group modal-footer">
    <?= Html::submitButton('<span class="glyphicon glyphicon-ok-circle"></span> Send ',[
        'class' => 'btn btn-success'
    ]);?>
    <button type="button" class="btn btn-success" data-dismiss="modal"><span
            class="glyphicon glyphicon-ban-circle"></span> Cancel
    </button>

</div>

<?php ActiveForm::end(); ?>
<?php Pjax::end(); ?>
</div>