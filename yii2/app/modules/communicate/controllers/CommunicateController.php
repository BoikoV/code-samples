<?php
namespace app\modules\communicate\controllers;

use Yii;
use common\models\backend\Message;
use app\modules\communicate\components\Controller;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\db\Expression;

class CommunicateController extends Controller
{
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Message::find()
            ->where(['from_id' => Yii::$app->session->get('__id') ])
            ->andWhere( ['thread_id' => null])
            ->orderBy(['new_replies'=>SORT_DESC,'date_sent'=>SORT_DESC]),

        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new Message(['scenario' => Message::SCENARIO_MESSAGE]);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
        } else {
            return $this->renderAjax('_form', [
                'model' => $model,
            ]);
        }
    }


    public function actionAnswer($id)
    {
        $dataProvider = new ActiveDataProvider([
             'query' => Message::find()
                 ->joinWith('creator')
                 ->where(['thread_id' => $id ])
                 ->orderBy(['date_sent'=>SORT_DESC]),

            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $bz=$dataProvider->getModels();
        $answer = new Message(['scenario' => Message::SCENARIO_ANSWER]);
        $model = $this->findModel($id);
        if (($model->to_id == Yii::$app->user->identity->getId()) && ($model->date_read=="NULL")){
            $model->date_read=new Expression('NOW()');
            $model->save();
        }

        $model->setScenario(Message::SCENARIO_NEW_ANSWER);

        if (($model->load(Yii::$app->request->post()) && $model->validate()) &&
           ($answer->load(Yii::$app->request->post()) && $answer->validate()))
        {
            $answer->save();
            $model ->save();

        } else {
            return $this->renderAjax('_answer_form', [
                'model' => $model,
                'answer' => $answer,
                'dataProvider' => $dataProvider,
            ]);
        }



    }
    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Message the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Message::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
