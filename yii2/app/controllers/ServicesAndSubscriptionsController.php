<?php
namespace app\controllers;

use Yii;
use common\components\Stripe;
use common\models\backend\Order;
use app\components\Controller;
use yii\web\NotFoundHttpException;

/**
 * ServicesAndSubscriptions
 */
class ServicesAndSubscriptionsController extends Controller
{
    public function actionIndex()
    {
        $order = $this->findOrder();
        $customer = Stripe::getCustomer($order->stripe_customer);

        return $this->render('index', [
            'order' => $order,
            'package' => $order->getPackageObject(),
            'subscription' => $order->getSubscriptionObject(),
            'customer' => $customer
        ]);
    }

    public function actionCancelSubscription()
    {
        $order = $this->findOrder();
//        if (Stripe::HasSubscription($order->stripe_customer)) {
            $order->cancelSubscription();
//            if (Stripe::CancelSubscription($order->stripe_customer)) {
                $this->redirect('index');
//            }
//        }
    }

    public function actionPaymentCard()
    {
        $order = $this->findOrder();
        $customer = Stripe::getCustomer($order->stripe_customer);
        if (count($customer->sources->data) == 0) {
            return $this->redirect('payment-card-add');
        }

        return $this->render('payment-card', [
            'order' => $order,
            'customer' => $customer
        ]);
    }

    public function actionPaymentCardAdd()
    {
        $model = $this->findOrder();
        $model->setScenario(Order::SCENARIO_ADD_CARD);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Stripe::AddCard($model->stripe_customer, $model->stripe_token);

            return $this->redirect('payment-card');
        }

        return $this->render('payment-card-add', [
            'model' => $model,
        ]);
    }

    public function actionPaymentCardDelete()
    {
        $order = $this->findOrder();
        Stripe::DeleteCard($order->stripe_customer);

        return $this->redirect('payment-card');
    }

    /**
     * @return null|Order
     * @throws NotFoundHttpException
     */
    protected function findOrder()
    {
        if (($model = Order::findOne(['user_id' => Yii::$app->user->identity->getId()])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The order does not exist.');
        }
    }
}
