<?php

/* @var $this yii\web\View */

$this->title = 'Licence & Privacy Agreements';

$parentCompany = 'Parent Company';
$company = 'company';
?>

<style>
    .license-agreement {
        height: 500px;
        overflow-y: scroll;
    }
</style>

<div class="site-index">
    <h1>Welcome to Keepmore.net</h1>

    <div class="body-content">
        <div class="row">
            <div class="col-lg-12">
                <p>
                    <strong>Oh, one last thing.</strong> You need to agree to our End User License Agreement
                    and Privacy Policy (yes, the fine print). Please read through it and if you have any
                    questions, contact us at <strong> <a
                            href="mailto:help@keepmore.net">help@keepmore.net</a></strong><a
                        href="mailto:help@keepmore.net"> </a></p>
            </div>
            <div class="col-lg-12">
                <h2>License Agreement: (required)</h2>

                <div class="license-agreement">
                    <p>
                        PLEASE READ THIS AGREEMENT CAREFULLY
                    </p>

                    <p>
                        End User License Agreement
                    </p>

                    <p>
                        KeepMore, Inc. ("KeepMore") is willing to license access to and use of KeepMore's "KeepMore.net"
                        financial tracking software and associated documentation (individually and collectively, the
                        "Software") to you and the company on whose behalf or as whose agent you are acquiring use of
                        the Software ("Licensee") only on the condition that Licensee accepts all of the terms in this
                        End User License Agreement (the "Agreement"). If you are an employee or agent of a company and
                        are entering into this Agreement to license the Software for use by the company for its own
                        business purposes, you hereby agree that you enter into this Agreement on behalf of the company
                        and that you have the authority to bind the company to the terms and conditions of this
                        Agreement. KeepMore recommends that Licensee print a copy of this Agreement and retain such copy
                        for Licensee's records and future reference.
                    </p>

                    <p>
                        KeepMore is willing to license access and use of the Software only to licensees and business in
                        the United States. If at the time of registration Licensee is an individual or business that is
                        a resident of any country outside of the United States, then Licensee must discontinue the
                        registration process and any amounts already paid, if any, by Licensee will be refunded by
                        KeepMore.
                    </p>

                    <p>
                        BY CLICKING ON THE "ACCEPT" BUTTON BELOW, LICENSEE ACKNOWLEDGES THAT IT HAS READ THIS AGREEMENT,
                        UNDERSTANDS IT AND AGREES TO BE BOUND BY IT. IF LICENSEE DOES NOT AGREE TO ANY OF THE TERMS
                        BELOW, <span style='text-transform:uppercase'>KeepMore</span> IS UNWILLING TO LICENSE THE
                        SOFTWARE TO LICENSEE, AND LICENSEE SHOULD CLICK ON THE "DECLINE" BUTTON BELOW TO DISCONTINUE THE
                        REGISTRATION PROCESS. IN SUCH CASE, ANY AMOUNTS, IF ANY, ALREADY PAID BY LICENSEE WILL BE
                        REFUNDED BY <span style='text-transform:uppercase'>KeepMore</span>.
                    </p>

                    <b>1. LICENSE.</b>

                    <p>
                        Subject to the terms and conditions of this Agreement, KeepMore hereby grants
                        Licensee a limited, non-exclusive, non-transferable personal license to access and use the
                        Software, in machine-readable form only, solely: (a) for Licensee's internal use for a single
                        business entity (each separate business entity must register for a separate "Account"); (b) on a
                        per-Account basis; and (c) as hosted by KeepMore through KeepMore's application Web site (the
                        "KeepMore Site") and through Accounts established by KeepMore for Licensee's use in accordance
                        with KeepMore's access and use policies (as updated and amended from time to time). Licensee
                        may, subject to the terms and conditions of this Agreement, designate its accountant or
                        financial advisor as users authorized to access and review Licensee's records, provided that any
                        such authorized accountants or financial advisors agree to and abide by the terms and conditions
                        of this Agreement.
                    </p>

                    <b>2. RESTRICTIONS.</b>

                    <p>
                        Licensee acknowledges and agrees that Licensee's use of the Software is subject
                        to the terms and conditions of this Agreement and KeepMore's privacy policy (the "Privacy
                        Policy"), as presented during the registration process on the KeepMore Site. Licensee must use
                        the Software in accordance with the minimum specifications set forth on the KeepMore Site, as
                        updated from time to time. Licensee must not access, use or copy the Software, in whole or in
                        part, except as expressly provided in this Agreement. Licensee must not modify, reproduce,
                        create derivative works of, distribute, sell, resell, lend, loan, lease, license, sublicense or
                        transfer Licensee's rights to the Software or any portion thereof. Licensee must not reverse
                        engineer, disassemble, decompile, or translate the Software, or otherwise attempt to derive the
                        source code of or any trade secrets embodied in the Software, or authorize any third party to do
                        any of the foregoing, except to the extent allowed under any applicable law. The Software
                        contains or may contain trade secrets, trademarks, patents, and copyrights owned by KeepMore,
                        and Licensee will maintain the Software (and all passwords or user IDs used to access and use
                        the Software) in strict confidence. Licensee must not allow any access to or use of the Software
                        by anyone other than Licensee's employees, and any such use must be consistent with the terms,
                        conditions and restrictions set forth in this Agreement. Any attempt to transfer any of the
                        rights, duties or obligations hereunder (including, without limitation, by disclosing user IDs
                        or passwords or otherwise allowing unauthorized use of the Software by third parties) not in
                        accordance with the foregoing is null and void and without any force or effect, and constitutes
                        a material breach of this Agreement.
                    </p>

                    <b>3. MAINTENANCE AND SUPPORT.</b>

                    <p>
                        (a) Subject to the terms of this Agreement and provided that Licensee is current on all fees for
                        each Account controlled by Licensee, KeepMore will provide to Licensee all error corrections,
                        bug fixes, updates, and other maintenance releases of the Software as KeepMore may make
                        generally available to other licensees of the Software ("Releases"). All Releases are "Software"
                        subject to this Agreement. Licensee acknowledges and agrees that KeepMore is under no obligation
                        to release any Releases to the Software and that KeepMore is under no obligation to modify the
                        Software to operate on any updated versions of operating systems or platforms. KeepMore reserves
                        the right to define any addition of a major element to the Software as a new product ("New
                        Product") and not a Release. Any such New Product shall not be provided to Licensee under this
                        Agreement and will be made available to Licensee only pursuant to a separate and mutually agreed
                        to and executed license agreement, which may include additional fees and terms.
                    </p>

                    <p>(b) Subject to the terms of this Agreement and provided that Licensee is current on all fees for
                        each Account controlled by Licensee, KeepMore will provide to Licensee KeepMore's standard
                        technical support for the Software (which may be by email or otherwise), as set forth on the
                        KeepMore Site and revised from time to time. Such support will be provided during the times and
                        according to the conditions of KeepMore's then-current terms of support.
                    </p>

                    <b>4. PAYMENT.</b>

                    <p>
                        Licensee's use of the Software is conditioned upon payment in full of the initial
                        license fees and on-going services fees charged by KeepMore in accordance with the payment
                        provisions set forth on the KeepMore Site as part of the registration process (the "Payment
                        Terms"), which Licensee agrees to review as part of the registration process. Such fees include
                        any applicable per-Account activation and registration fees and all monthly fees and services
                        charges applicable to such account, as established by the Payment Terms. KeepMore reserves the
                        right to modify and amend (including, without limitation, by decreasing or increasing) the fees
                        charged to Licensee and Payment Terms, provided that KeepMore provides Licensee at least thirty
                        (30) days' written (including via email) notice of such changes.
                    </p>

                    <b>5. TERM AND TERMINATION.</b>

                    <p>(a) This Agreement will be deemed to have commenced upon the date on which Licensee accepts the
                        terms and conditions of this Agreement by clicking on the "ACCEPT" button or otherwise accessing
                        and using the Software, and, unless extended by written agreement of the parties, this Agreement
                        will expire upon the earlier of: (i) written notice from KeepMore upon Licensee's material
                        breach of any term or condition of this Agreement if such breach is not cured within 30 days of
                        KeepMore's written notice thereof to Licensee; or; (ii) written notice from Licensee of
                        termination for any reason.
                    </p>

                    <p>(b) Upon termination or expiration of this Agreement for any reason, the license set forth in
                        Section 2 will terminate and Licensee must immediately cease all access to and use of the
                        Software and either return or destroy the Software (if any has been provided to Licensee
                        hereunder), all passwords and user IDs used to access the Software and all other information
                        related thereto.
                    </p>

                    <p>(c) Sections 2 and 6 through 12 will survive any expiration or termination of this Agreement.
                    </p>

                    <b>6. OWNERSHIP.</b>

                    <p>
                        The Software is licensed, not sold, to Licensee for use only under the terms of
                        this Agreement, and KeepMore reserves all rights not expressly granted to Licensee. Under no
                        circumstances will Licensee be deemed to receive title to or ownership of any portion of the
                        Software, any Releases or New Products made available by KeepMore or any modifications or
                        derivative works thereof, or any intellectual property rights thereto (including, without
                        limitation, copyrights, patents and trade secrets incorporated or embodied therein), title to
                        and ownership of which at all times will remain vested exclusively in KeepMore.
                    </p>

                    <b>7. TAX-RELATED</b>

                    <p>
                        INFORMATION. Licensee must review its business financial records for indications
                        of obvious errors prior to using such information for any purpose. Additionally, accounting and
                        tax laws and regulations change frequently and their application can vary widely based upon the
                        specific facts and circumstances involved. Licensee is encouraged to consult with its own
                        professional accounting and tax advisors concerning specific accounting and tax circumstances.
                        KeepMore expressly disclaims any responsibility for the accuracy or adequacy of any accounting
                        or tax positions taken by Licensee with respect to accounting or tax records.
                    </p>

                    <b>8. WARRANTY AND DISCLAIMERS.</b>

                    <p>
                        (a) Upon completion of the registration process (including, without limitation, payment of
                        initial fees and execution of this Agreement), KeepMore will deliver to Licensee a hardcopy of
                        this Agreement and supporting materials ("Confirmation Materials"). LICENSEE SHOULD RETAIN THE
                        CONFIRMATION MATERIALS FOR LICENSEE'S RECORDS. If for any reason Licensee is not fully satisfied
                        with the Software within thirty (30) days of the date on which Licensee registers for use of the
                        Software (the "Warranty Period"), then Licensee may return to KeepMore at the address set forth
                        on the KeepMore Site, postage prepaid by Licensee, the Confirmation Materials, and KeepMore will
                        refund the fees paid by Licensee for the Software (including, without limitation, any annual
                        fees paid in advance by Licensee, provided such return package is postmarked prior to the last
                        day of the Warranty Period). IF LICENSEE DOES NOT RETURN THE CONFIRMATION MATERIALS TO <span
                            style='text-transform:uppercase'>KeepMore</span> BEFORE THE END OF THE WARRANTY PERIOD, THEN
                        NO REFUND WILL BE DUE.
                    </p>

                    <p>
                        (b) SUBJECT TO THE FOREGOING LIMITED WARRANTY, THE SOFTWARE IS PROVIDED TO LICENSEE STRICTLY "AS
                        IS," AND <span style='text-transform:uppercase'>KeepMore</span> AND ITS SUPPLIERS AND LICENSORS
                        EXPRESSLY DISCLAIM ANY AND ALL WARRANTIES AND REPRESENTATIONS OF ANY KIND WITH REGARD TO ANY
                        SUBJECT MATTER OF THIS AGREEMENT, INCLUDING, WITHOUT LIMITATION, ANY WARRANTY OF
                        NON-INFRINGEMENT, TITLE, FITNESS FOR A PARTICULAR PURPOSE, FUNCTIONALITY OR MERCHANTILIABILITY,
                        ACCURACY OF RESULTS OR INFORMATION, WHETHER EXPRESS, IMPLIED OR STATUTORY. <span
                            style='text-transform:uppercase'>KeepMore</span> MAKES NO WARRANTY THAT THE SOFTWARE OR USE
                        THERE OF WILL BE UNINTERRUPTED, ERROR-FREE OR SECURE. NO ORAL OR WRITTEN INFORMATION OR ADVICE
                        GIVEN BY <span style='text-transform:uppercase'>KeepMore</span>, ITS EMPLOYEES, DISTRIBUTORS,
                        DEALERS, OR AGENTS WILL INCREASE THE SCOPE OF THE ABOVE WARRANTIES OR CREATE ANY NEW WARRANTIES.
                    </p>

                    <b>9. LIMITATION OF REMEDIES.</b>

                    <p>
                        REGARDLESS OF WHETHER ANY REMEDY SET FORTH HEREIN FAILS OF ITS
                        ESSENTIAL PURPOSE OR OTHERWISE, IN NO EVENT WILL <span
                            style='text-transform:uppercase'>KeepMore</span> OR ITS SUPPLIERS BE LIABLE TO LICENSEE OR
                        TO ANY THIRD PARTY FOR ANY LOST PROFITS, LOST DATA, INTERRUPTION OF BUSINESS, OR OTHER SPECIAL,
                        INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND ARISING OUT OF THE USE OR INABILITY TO
                        USE THE SOFTWARE OR ANY DATA SUPPLIED THEREWITH, EVEN IF <span style='text-transform:uppercase'>KeepMore</span>
                        HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH LOSS OR DAMAGES AND WHETHER OR NOT SUCH LOSS OR
                        DAMAGES ARE FORESEEABLE. IN NO EVENT WILL THE LIABILITY OF <span
                            style='text-transform:uppercase'>KeepMore</span> EXCEED THE TOTAL AMOUNT ACTUALLY PAID TO
                        <span style='text-transform:uppercase'>KeepMore</span> FROM LICENSEE FOR THE SOFTWARE. Each
                        party expressly acknowledges and agrees that the foregoing limitations of liability and
                        allocation of risk reflect part of the bargained-for exchange of the parties with respect to
                        this Agreement and the Software.
                    </p>

                    <b>10. U. S. GOVERNMENT END USERS.</b>

                    <p>
                        The Software was developed at private expense and is a
                        "commercial item" as that term is defined at 48 C.F.R. 2.101, consisting of "commercial computer
                        software" and "commercial computer software documentation" as such terms are used in 48 C.F.R.
                        12.212. Consistent with 48 C.F.R. 12.212 and 48 C.F.R. 227.7202-1 through 227.7202-4, all U.S.
                        Government end users acquire the Software with only those limited rights set forth therein.
                        Publisher is KeepMore, Inc., 5757 Central Avenue, Suite K, Boulder, Colorado 80301.
                    </p>

                    <b>11. HIGH RISK ACTIVITY.</b>

                    <p>
                        Licensee acknowledges and agrees that the Software is not intended for
                        use with any high risk or strict liability activity, including, without limitation, air or space
                        travel, technical building or structural design, power plant design or operation, life support
                        or emergency medical operations or uses, and that KeepMore makes no warranty and will have no
                        liability arising from any use of the Software in any high risk or strict liability activities.
                    </p>

                    <b>12. GENERAL.</b>

                    <p>
                        This Agreement will be governed by the laws of the State of Colorado and the United
                        States, without regard to or application of conflicts of law rules or principles that would
                        require the application of the laws of another jurisdiction. The parties explicitly disclaim the
                        application of the UN Convention on the Sale of Goods. Any action or lawsuit related to this
                        Agreement must be brought exclusively in either the federal or state courts located in the City
                        and County of Denver, and each party hereby irrevocably submits and waives any objection to the
                        exclusive jurisdiction and forum of such courts. If any provision of this Agreement is held to
                        be unenforceable, that provision will be reformed in order to comply with the law and to the
                        extent possible give effect to the original intent and economic impact of the original
                        provision, and the remaining provisions will remain in full force. The prevailing party in any
                        lawsuit or proceeding arising from or related to this Agreement will be entitled to receive its
                        costs, expert witness fees and reasonable attorneys' fees, including costs and fees on appeal.
                        The failure of either party to require performance by the other party of any provision hereof
                        will not affect the full right to require such performance at any time thereafter, nor will the
                        waiver by either party of a breach of any provision hereof be taken or held to be a waiver of
                        the provision itself. Neither this Agreement nor any rights or obligations of Licensee hereunder
                        may be assigned, delegated or transferred by Licensee (in whole or in part and including by
                        sale, merger or operation of law) without the prior written approval of KeepMore, and any such
                        transfer in derogation of the foregoing will be null and void. The parties hereto are
                        independent parties and neither acquires hereunder any right or ability to bind or enter into
                        any obligation on behalf of the other. This Agreement, together with the Privacy Policy and
                        Payment Terms, which are hereby referenced and incorporated, is the complete and exclusive
                        statement of the agreement between the parties, and this Agreement supersedes any proposal or
                        prior agreement, oral or written, and any other communications between the parties in relation
                        to the subject matter of this Agreement. This Agreement will not be modified except by a
                        subsequently dated written amendment or exhibit signed by both parties by their duly authorized
                        representatives.
                    </p>

                    <p>
                        The Software consists of material that is � Copyright 1996 - 2003, KeepMore, Inc. All rights
                        reserved. Protected by copyright and licenses restricting use, copying, distribution and
                        decompilation. KeepMore is a trademark of KeepMore, Inc.
                    </p>
                </div>
            </div>
            <div class="col-lg-12">
                <h2>Privacy Policy: (required)</h2>

                <div class="license-agreement">
                    <p>
                        At KeepMore, we are committed to respecting your privacy and the security of your data.
                    </p>

                    <p>
                        Throughout this privacy policy (the "Policy"), we refer to information that personally
                        identifies
                        you or your business (e.g., email address, name, age, etc.) as 'personal information'.
                    </p>

                    <p>
                        This policy applies to users who are current or former KeepMore customers.
                    </p>

                    <p>
                        1. We do not sell your personal information to third parties.
                    </p>

                    <p>
                        2. We adhere to industry standards with respect to the protection of the security and
                        confidentiality of your personal information. Our approach to privacy and how your personal
                        information is collected, used and secured is set forth below.
                    </p>

                    <p>
                        <b>REMEMBER</b> - Access to and use of the KeepMore system and software is subject to the terms and
                        conditions of KeepMore's end user license agreement. The following Policy sets forth only
                        KeepMore's
                        policies with respect to privacy and security of certain information and data. KeepMore's end
                        user
                        license agreement governs access and use of KeepMore software and systems in all other respects.
                        In
                        the event of any inconsistency or ambiguity between this Policy and KeepMore's end user license
                        agreement, the end user license agreement shall control.
                    </p>

                    <p>
                        1. We do not sell your personal information to third parties. We use personal information as set
                        forth in this Policy.
                    </p>

                    <p>
                        2. KeepMore only shares personal information with third parties under limited circumstances:
                    </p>

                    <p>
                        - Both technical and non-technical companies may assist us in supporting your customer service
                        needs, and we may disclose personal information to these companies. Our contracts with such
                        companies prohibit them from using any of your personal information for their own purposes and
                        also
                        require that personal information provided to them by us remain confidential.
                    </p>

                    <p>
                        - We may also enter into marketing relationships with other companies that may be able to offer
                        you
                        valuable services. Your personal information will not be disclosed to these companies, but
                        non-personal information may be used anonymously or in an aggregated form that does not identify
                        you
                        personally for developing these services.
                    </p>

                    <p>
                        - If you have affirmatively consented to the use of personal information or provided personal
                        information for specific uses or purposes.
                    </p>

                    <p>
                        - Where we believe in good faith that disclosure is required or permitted under law to cooperate
                        with regulators or law enforcement authorities, we may disclose or report your personal
                        information.
                    </p>

                    <p>
                        3. We collect and use personal information and non-personal information in the normal course of
                        business in order to administer your accounts and serve you better.
                    </p>

                    <p>
                        - Application and registration information. We collect information that you provide to us when
                        you
                        register for our software.
                    </p>

                    <p>
                        - Web site usage. We collect some information on our web sites through the use of "cookies." For
                        example, we may identify the pages on our web sites that your browser requests or visits.
                    </p>

                    <p>
                        - Transaction information. Once you have a registered as a user with us, to administer your
                        account
                        and better serve you, we collect and maintain personal information about your transactions,
                        including balances, and history.
                    </p>

                    <p>
                        - As part of our efforts to reach and serve new and existing clients we sometimes collect and
                        use
                        information from outside mailing lists.
                    </p>

                    <p>
                        4. We respect the confidentiality and security of your personal information.
                    </p>

                    <p>
                        - Companies we hire to provide support services are not allowed to use your personal information
                        for
                        their own purposes and are contractually obligated to maintain strict confidentiality. We limit
                        their use of your personal information to the performance of the specific service we (or you)
                        have
                        requested.
                    </p>

                    <p>
                        - We restrict access to personal information to only employees and agents who require access and
                        for
                        business purposes only.
                    </p>

                    <p>
                        - We maintain physical, electronic, and procedural safeguards to guard your personal
                        information.
                    </p>

                    <p>
                        5. We will provide notice of changes in our information-sharing practices.
                    </p>

                    <p>
                        - If, at any time in the future, it is necessary to disclose any of your personal information in
                        a
                        way that is inconsistent with this Policy, we will give you advance notice of the proposed
                        change so
                        that you will have the opportunity to opt-out of such disclosure.
                    </p>

                    <p>
                        - If you have any questions or concerns, please contact us by e-mail at help@keepmore.net.
                    </p>

                    <p>
                        Additional Information About Privacy and Security At KeepMore
                        The KeepMore Private and Secure web site:
                    </p>

                    <p>
                        KeepMore endeavors to provide a private and secure Web environment for entering and tracking
                        your
                        business activity. Logging-in to your account requires both a personal user name and a password
                        individually selected by you. Although no system is 100% tamper proof, our web site uses
                        encryption
                        technology so that the data we transmit to you and the data you transmit to us across the
                        Internet
                        is secure, including account transactions and balances and transaction activity and history and
                        messages sent to and from KeepMore's web site.
                    </p>

                    <p>
                        Browsers and Internet Security:
                    </p>

                    <p>
                        Any time you enter or request personal information on our secure site we encrypt it using Secure
                        Socket Layer (SSL) technology. SSL protects information as it crosses the Internet. To support
                        this
                        technology, you need an SSL-capable browser. KeepMore recommends using a strong encryption,
                        128-bit
                        browser like Netscape Navigator 4.06 or higher or Microsoft's Internet Explorer 4.01 or higher.
                        These browsers will activate SSL automatically whenever you sign on to your account. Under no
                        circumstances should you use any software, program, application, or any other device not
                        recommended
                        or approved by KeepMore in writing to access or log-in to the KeepMore system, software or web
                        site,
                        or to automate the process of obtaining, downloading, transferring, or transmitting any content
                        to
                        or from KeepMore's systems, software or web site. You can tell if you are visiting a secure area
                        within a Web site by looking at the symbol on the bottom of your browser screen. If you are
                        using
                        Netscape Navigator or Internet Explorer, you will see either a lock or a key. When the symbol
                        appears unbroken or the padlock is in the locked position, your session connection is taking
                        place
                        via a secure server.
                    </p>

                    <p>
                        If you need a strong encryption browser, you can go to the Netscape Web site or the Microsoft
                        Web
                        site to download the latest Navigator or Internet Explorer browser. We do not recommend the use
                        of
                        beta browser versions.
                    </p>

                    <p>
                        Your Password and Other Security Issues:
                    </p>

                    <p>
                        Your password is your private entry key into your account. Never share it with anyone and change
                        it
                        periodically. You can change your password any time you like after logging in to our secure web
                        site. Our password requirements, which facilitate online account security, are as follows:
                    </p>

                    <p>
                        Your password must be 6-8 characters long.
                    </p>

                    <p>
                        If you forget your password, contact us at help@KeepMore.net and we will send you a new password
                        that you can then change to a private password.
                    </p>

                    <p>
                        Logging Off:
                    </p>

                    <p>
                        After you've finished accessing your account, make sure to log off. This prevents someone else
                        from
                        accessing your account if you leave your computer and your session hasn't "timed out," or
                        automatically shut down.
                    </p>

                    <p>
                        Cookies, GIFs and Other Web Tools:
                    </p>

                    <p>
                        When you interact with a KeepMore online channel, such as one of our web sites or through email
                        or a
                        wireless device, we strive to make that experience easy and meaningful. To do so, we use several
                        tools. When you come to our web site, our web server sends a cookie to your computer. A cookie
                        is
                        simply an electronically transmitted file that holds small pieces of information. When you
                        navigate
                        through our web site, your browser "requests" pages for you to view, and that request will
                        include
                        the information stored in the cookie we previously sent to your computer. This process is like
                        an
                        electronic "handshake" between KeepMore's system and your computer; the information exchanged
                        allows
                        us to recognize your browser. KeepMore also uses other technology, such as "Graphics Interchange
                        Format" files (GIFs) to recognize users and collect and transmit information online, as
                        described
                        below.
                    </p>

                    <p>
                        Cookies and Personal Identification. Standing alone, cookies, GIFs and other web tools, as well
                        as
                        data derived from them, do not identify you personally. They merely recognize your browser.
                        Unless
                        you choose to identify yourself to KeepMore, either by responding to a promotional offer,
                        opening an
                        account, registering for an online service or logging on to your account, you remain anonymous
                        to
                        KeepMore.
                    </p>

                    <p>
                        E-mail Communications and Opting Out:
                    </p>

                    <p>
                        E-mail is an important communication channel between KeepMore and its customers. KeepMore also
                        uses
                        e-mail to communicate with prospective customers.
                    </p>

                    <p>
                        E-mail sent to KeepMore from within our secure site (after you have logged on with your user
                        name
                        and password) is secured with the same Secure Socket Layer (SSL) technology we use to transmit
                        private account information. So please remember that any e-mail you send to KeepMore identifying
                        your account should originate in the secure KeepMore site. Our news and information services may
                        be
                        delivered to you via e-mail from outside our secure site. You will receive these communications
                        via
                        unencrypted e-mail. At times we may ask for your informed consent to send important information
                        to
                        you by e-mail.
                    </p>

                    <p>
                        We may also use your e-mail address to send you information about services and products we
                        believe
                        may be of interest to you.
                    </p>

                    <p>
                        KeepMore Alliances and Promotions:
                    </p>

                    <p>
                        When KeepMore joins with another company to offer or provide products and services, both parties
                        may
                        cross-reference their databases to search for customers in common - while adhering to our
                        'personal
                        information' policy outlined above. This existing information may be used to help identify
                        business
                        alliance opportunities and to enhance the information already contained in the existing
                        databases.
                    </p>

                    <p>
                        KeepMore on its own, or jointly with other organizations and web sites, may offer contests,
                        sweepstakes, and promotions. You may be asked to supply certain personal information to
                        participate
                        in these promotional events, which may then be used by the sponsoring parties for marketing
                        purposes. Even if you have previously opted not to receive information or mailings about
                        KeepMore,
                        you may still receive information about KeepMore via these promotions if you choose to register
                        for
                        them. Rules and guidelines for each promotion will be clearly posted, as will notification about
                        how
                        the information gathered may be used. You may opt not to participate in these special promotions
                        if
                        you do not want to receive information about KeepMore or share your information with all
                        sponsoring
                        parties.
                    </p>

                    <p>
                        Account Registration Data:
                    </p>

                    <p>
                        Potential customers applying online at a KeepMore Web site for an account have the ability to
                        opt
                        out of the registration process before completing it. However, we may use the information
                        gathered
                        on the registration form during this process to send you further information about KeepMore
                        services
                        and products we believe may be of interest to you.
                    </p>

                    <p>
                        Correcting and Updating Your Information:
                    </p>

                    <p>
                        The accuracy of your personal information is important to us. If you are a customer and have a
                        concern about your personal or account information maintained at KeepMore, or want to correct,
                        update, or confirm your information, please contact us at help@keepmore.net.
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <?= \yii\helpers\Html::a('Accept', [
                    'agreements',
                    'accept' => 1
                ], ['class' => 'btn btn-success']) ?>
            </div>
        </div>
    </div>
</div>
