<div class="body-content-options">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <button class="btn btn-success plus" id="<?=$btnAddId ?>" data-url="<?= $urlAdd ?>"><i
                            class="fa fa-plus" aria-hidden="true"> </i> Add
                    </button>
                </div>
            </div>
            <div class="col-md-6 actions">
                <button title="Update" data-action="update" disabled="disabled" class="btn btn-action"
                        type="button">
                    <span class="action edit"></span>
                </button>
                <button title="Delete" data-action="delete" disabled="disabled" class="btn btn-action"
                        type="button">
                    <span class="action delete"></span>
                </button>
            </div>
        </div>
    </div>
</div>
