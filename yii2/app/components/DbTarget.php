<?php

namespace app\components;

use yii\db\Connection;
use yii\di\Instance;

class DbTarget extends \yii\log\DbTarget
{
    public function init()
    {
        /** @var Connection $connection */
        $connection = \Yii::$app->app_db;
        $connection->dsn = str_replace('{dbname}', \Yii::$app->user->id, $connection->dsn);
        $this->db = Instance::ensure($this->db, Connection::className());
    }
}
