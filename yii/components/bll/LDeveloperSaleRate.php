<?php

class LDeveloperSaleRate extends DeveloperSaleRate
{
    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';

    public $rate_low, $rate_avg, $rate_high, $currency;

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array(
                'user_id, rate_low, rate_avg, rate_high, planned_day_time, currency, date',
                'required',
            ),
            array(
                'user_id',
                'exist',
                'attributeName' => 'id',
                'className' => 'User',
                'message' => 'Разработчик обязателен'
            ),
            array(
                'currency',
                'in',
                'range' => array_keys(AppHelper::prepareList([Currency::getDefaultSystemCurrency()], 'id', 'name')),
            ),
            array(
                'rate_low, rate_avg, rate_high',
                'numerical',
                'min' => 1,
                'max' => 100,
            ),
            array(
                'planned_day_time',
                'numerical',
                'min' => 1,
                'max' => 20,
            ),
            array('date', 'date', 'format' => 'dd.MM.yyyy'),
            array('date', 'dateValidate'),
            array('comment, date', 'safe'),
        );
    }

    public function dateValidate($attribute, $params)
    {
        if (!$this->hasErrors('user_id')) {
            $criteria = new CDbCriteria();
            $criteria->order = 'date desc';
            $criteria->addCondition('user_id=:user_id');
            $criteria->params = [
                ':user_id' => $this->user_id,
            ];
            if ($result = self::model()
                ->find($criteria)
            ) {
//                if ($maxDate = AppHelper::detectDateTime($result->date)) {
//                    $maxDate->modify('first day of this month');
//                    $newDate = AppHelper::dateFromFormatApplication($this->date)
//                        ->modify('first day of this month');
//                    if ($result->id != $this->id && $newDate->getTimestamp() <= $maxDate->getTimestamp()) {
//                        $this->addError($attribute,
//                            'Дата не может быть раньше действушей даты: ' . AppHelper::formatDateApplication($maxDate));
//                    } elseif (!$this->canBeChanged($newDate)) {
//                        $this->addError($attribute, 'Не возможно добавить/изменить закрытый период');
//                    }
//                }
            }
        }
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), array(
            'rate_low' => 'Нижний рейт',
            'rate_avg' => 'Средний рейт',
            'rate_high' => 'Верхний рейт',
            'currency' => 'Валюта',
        ));
    }

    public function afterFind()
    {
        $this->rate_low = $this->rateLowTransaction->money;
        $this->rate_avg = $this->rateAvgTransaction->money;
        $this->rate_high = $this->rateHighTransaction->money;
        $this->currency = $this->rateLowTransaction->cur_id;

        return parent::afterFind();
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return LDeveloperSaleRate the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function set()
    {
        if (!$this->validate()) {
            return false;
        }
        $transaction = Yii::app()->db->beginTransaction();
        $result = false;
        try {
            if ($this->isNewRecord) {
                $moneyTransactionLowRate = MoneyTransaction::set($this->currency, $this->rate_low);
                $moneyTransactionAvgRate = MoneyTransaction::set($this->currency, $this->rate_avg);
                $moneyTransactionHighRate = MoneyTransaction::set($this->currency, $this->rate_high);
                $this->rate_low_transaction_id = $moneyTransactionLowRate->id;
                $this->rate_avg_transaction_id = $moneyTransactionAvgRate->id;
                $this->rate_high_transaction_id = $moneyTransactionHighRate->id;
            } else {
                if ($this->rateLowTransaction->money != $this->rate_low
                    || $this->rateLowTransaction->cur_id != $this->currency
                ) {
                    MoneyTransaction::set($this->currency, $this->rate_low, $this->rateLowTransaction->id);
                }
                if ($this->rateAvgTransaction->money != $this->rate_avg
                    || $this->rateAvgTransaction->cur_id != $this->currency
                ) {
                    MoneyTransaction::set($this->currency, $this->rate_avg, $this->rateAvgTransaction->id);
                }
                if ($this->rateHighTransaction->money != $this->rate_high
                    || $this->rateHighTransaction->cur_id != $this->currency
                ) {
                    MoneyTransaction::set($this->currency, $this->rate_high, $this->rateHighTransaction->id);
                }
            }
            if ($this->save()) {
                $result = true;
                $transaction->commit();
            }
        } catch (Exception $ex) {
        }
        if (!$result) {
            $transaction->rollback();
        }

        return true;
    }

    public function delete()
    {
        $result = false;
        if ($this->canBeChanged()) {
            $transaction = Yii::app()->db->beginTransaction();
            try {
                if (parent::delete()
                    && $this->rateLowTransaction->delete()
                    && $this->rateAvgTransaction->delete()
                    && $this->rateHighTransaction->delete()
                ) {
                    $result = true;
                }
            } catch (Exception $ex) {
            }
            if ($result) {
                $transaction->commit();
            } else {
                $transaction->rollback();
            }
        }

        return $result;
    }

    /**
     * @param $page
     * @param $limit
     * @param $sortId
     * @param $sortOrder
     * @param $employeeId
     * @return Portion
     */
    public static function getPortion($page, $limit, $sortId, $sortOrder, $employeeId = 0)
    {
        $criteria = new CDbCriteria();
        $criteria->alias = 'DeveloperSaleRate';
        if (!Yii::app()->user->isAuthority && !Yii::app()->user->isHeadManager && !Yii::app()->user->isAccountant) {
            if ($employeeId) {
                $subordinates = AppHelper::prepareList(LManager::getSubordinates(), 'id', function ($model) {
                    return $model->getLongName();
                });
                if (!array_key_exists($employeeId, $subordinates)) {
                    $employeeId = 0;
                }
            }
        }
        $criteria->addCondition('DeveloperSaleRate.user_id=:employee_id');
        $criteria->params[':employee_id'] = $employeeId;
        $criteria->group = 'DeveloperSaleRate.id';
        $count = self::model()
            ->count($criteria);
        if ($count > 0) {
            $totalPages = ceil($count / $limit);
        } else {
            $totalPages = 0;
        }
        if ($page > $totalPages) {
            $page = $totalPages;
        }
        $start = ($limit * $page) - $limit;
        $criteria->order = 'DeveloperSaleRate.' . $sortId . ' ' . $sortOrder;
        $criteria->limit = $limit;
        $criteria->offset = $start;
        $models = self::model()
            ->findAll($criteria);

        return new Portion($models, $page, $totalPages, $count);
    }

    /**
     * @param $page
     * @param $limit
     * @param $sortId
     * @param $sortOrder
     * @return Portion
     */
    public static function getAllEmployeesPortion($page, $limit, $sortId, $sortOrder)
    {
        $actualRates = Yii::app()->db->createCommand()
            ->select('id')
            ->from('('.Yii::app()->db->createCommand()
                ->select('user_id, max(date) as date')
                ->from((new DeveloperSaleRate())->tableName())
                ->group('user_id')
                ->getText().') as t')
            ->join((new DeveloperSaleRate())->tableName() . ' as dsr', 'dsr.user_id=t.user_id and dsr.date=t.date')
            ->queryColumn()
        ;
        $criteria = new CDbCriteria();
        $criteria->alias = 'DeveloperSaleRate';
        $criteria->with = ['user', 'rateLowTransaction', 'rateAvgTransaction', 'rateHighTransaction'];
        $isSuperUser = Yii::app()->user->isAuthority || Yii::app()->user->isHeadManager
            || Yii::app()->user->isSalesManager
            || Yii::app()->user->isAccountant;
        if (!$isSuperUser) {
            $subordinates = CHtml::listData(LManager::getSubordinates(), 'id', 'id');
            $criteria->addInCondition('DeveloperSaleRate.user_id', array_keys($subordinates));
        }
        $criteria->addInCondition('DeveloperSaleRate.id', $actualRates);
        $criteria->group = 'DeveloperSaleRate.id';
        $count = self::model()
            ->count($criteria);
        if ($count > 0) {
            $totalPages = ceil($count / $limit);
        } else {
            $totalPages = 0;
        }
        if ($page > $totalPages) {
            $page = $totalPages;
        }
        $start = ($limit * $page) - $limit;
        $order = 'DeveloperSaleRate.' . $sortId;
        if ($sortId == 'employee') {
            $order = 'user.first_name';
        } elseif ($sortId == 'rate_low_transaction') {
            $order = 'rateLowTransaction.money';
        } elseif ($sortId == 'rate_avg_transaction') {
            $order = 'rateAvgTransaction.money';
        } elseif ($sortId == 'rate_high_transaction') {
            $order = 'rateHighTransaction.money';
        }
        $criteria->order = $order . ' ' . $sortOrder;
        $criteria->limit = $limit;
        $criteria->offset = $start;
        $models = self::model()
            ->findAll($criteria);

        return new Portion($models, $page, $totalPages, $count);
    }

    /**
     * @return Currency
     */
    public function getCurrency()
    {
        return $this->rateHighTransaction->cur;
    }

    public static function createAllowed()
    {
        return Yii::app()->user->isAuthority;
    }

    public function updateAllowed()
    {
        return Yii::app()->user->isAuthority;
    }

    public function deleteAllowed()
    {
        return Yii::app()->user->isAuthority;
    }

    public function canBeChanged(DateTime $period = null)
    {
//        if (!$period) {
//            $period = AppHelper::detectDateTime($this->date);
//        }
//
//        return !$this->user->hasLockedPeriods($period);

        return true;
    }
}
