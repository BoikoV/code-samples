<?php

class Portion
{
    public $models, $page, $totalPages, $count;

    public function __construct(array $models, $page, $totalPages, $count)
    {
        $this->models = $models;
        $this->page = $page;
        $this->totalPages = $totalPages;
        $this->count = $count;
    }
}
